<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', function () {
    return view('welcome');
})->middleware('auth');
Route::group(['prefix' => '', 'middleware' => 'auth'], function() {
Route::resource('/Direction','Directions\DirectionsController');
Route::resource('/DirectionT','DirectionsT\DirectionsTController');
Route::resource('/Departement','Departements\DepartementsController');
Route::resource('/Office','Offices\OfficesController');
Route::resource('/Fonction','Fonctions\FonctionsController');
Route::resource('/Courrier','Courriers\CourriersController');
Route::resource('/Type','Types\TypesController');
Route::resource('/Nature','Natures\NaturesController');
Route::resource('/Etat','Etats\EtatsController');
Route::resource('/Priorite','Priorites\PrioritesController');
Route::resource('/Chrono','Chronos\ChronosController');
Route::resource('/ChronosArchivage','ChronosArchive\ChronosArchiveController');
Route::resource('/Role','Roles\RolesController');
Route::resource('/Affectation','Affectations\AffectationsController');
Route::resource('/Redirection','Redirection\RedirectionController');
Route::resource('/Archivage','Archivages\ArchivagesController');


Route::resource('/Reponse','Reponse\ReponseController');
Route::get('/pdf/{order}', ['as' => 'order.pdf', 'uses' => 'ReponseController@reponsePdf']);
Route::get('/archive/{id}', 'Courriers\CourriersController@archive')->name('archive');
Route::delete('/supprimecourrier/{id}', 'Courriers\CourriersController@supprimecourrier')->name('Courrier.destroy1');

Route::post('/supprimecourrier2/{id}', 'Courriers\CourriersController@supprimecourrier')->name('Courrier.destroy2');

Route::get('/home', 'HomeController@index')->name('home');
});
Route::get('/getresetpassword','Users\UsersController@getresetpassword')->name('getresetpassword');
Route::post('/postresetpassword','Users\UsersController@postresetpassword')->name('postresetpassword');
Route::resource('/User','Users\UsersController');
Auth::routes();
Route::get('/Close/{affect_id}/{etat_id}/{courrier_id}', 'Courriers\CourriersController@close')->name('close');

Route::get('/profile/{id}','Users\UsersController@show');
Route::get('/deconnexion','Users\UsersController@deconnexion');

Route::get('/statut/{id}', 'Users\UsersController@statut')->name('statut');
Route::get('/archivedef/{numero}', 'Archivages\ArchivagesController@archivedef')->name('archivedef');
Route::get('/delete/{id}', 'Archivages\ArchivagesController@delete')->name('delete');
Route::get('/remettre/{id}', 'Archivages\ArchivagesController@remettre')->name('remettre');


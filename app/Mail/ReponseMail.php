<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Model\Piecesjointes;

class ReponseMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $reponse;
    public $reponse_id;

    public function __construct($reponse, $reponse_id)
    {
        $this->reponse = $reponse;
         $this->reponse_id = $reponse_id;


            $idm = $reponse_id; 
            $files = Piecesjointes::where('reponse_id', $idm)->first();
            if (!is_null($files) && !empty($files)) {
                $file = $files->libelle; 
            }
            else{
               $file = '';
            }
            
        
    }


 

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
          
            $idm =  $this->reponse_id; 
            $files = Piecesjointes::where('reponse_id', $idm)->orderBy('id','asc')->first();
            if (!is_null($files)) {
                $file = $files->libelle; 
                if (!empty($file)) {
                     return $this->markdown('emails.emails.reponse-mail')
                    ->attachFromStorage($file, 'Reponse.pdf', [
                   'mime' => 'application/pdf'
                    ]);
                }
            } 
            else{
               return $this->markdown('emails.emails.reponse-mail');
            }
       
    }
}

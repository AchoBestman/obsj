<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Departements extends Model
{
    protected $guarded = ['id'];

     public function direction()
    {
    	return $this->belongsTo('App\Model\DirectionsT');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Affectations extends Model
{
    protected $guarded = ['id'];

     public function courrier()
    {
    	return $this->belongsTo('App\Model\Courriers');
    }

    public function qui()
    {
    	return $this->belongsTo('App\Model\Users','qui','id');
    }

     public function aqui()
    {
    	return $this->belongsTo('App\Model\Users','aqui','id');
    }

     public function etat()
    {
        return $this->belongsTo('App\Model\Etats');
    }



}

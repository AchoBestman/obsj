<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\Model\Users');
    }

    public function nature()
    {
    	return $this->belongsTo('App\Model\Natures');
    }


    public function type()
    {
    	return $this->belongsTo('App\Model\Types');
    }

}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DirectionsT extends Model
{
     protected $guarded = ['id'];

       public function direction()
    {
    	return $this->belongsTo('App\Model\Directions');
    }
}

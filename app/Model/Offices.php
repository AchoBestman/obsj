<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Offices extends Model
{
     protected $guarded = ['id'];

     public function departement()
    {
    	return $this->belongsTo('App\Model\Departements');
    }
}
 
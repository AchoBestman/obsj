<?php

namespace App\Model;
use App\Model\Fonctions;
use App\Model\Offices;
use App\Model\Roles;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
   protected $guarded = ['id'];

    public function fonction()
    {
    	return $this->belongsTo('App\Model\Fonctions');
    }

    public function office()
    {
    	return $this->belongsTo('App\Model\Offices');
    }

    public function role()
    {
    	return $this->belongsTo('App\Model\Roles');
    }
} 

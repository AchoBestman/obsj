<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Courriers extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\Model\Users');
    }

    public function expediteur()
    {
    	return $this->belongsTo('App\Model\Users','expediteur_id','id');
    }

    public function destinataire()
    {
    	return $this->belongsTo('App\Model\Users','destinataire_id','id');
    }


    public function nature()
    {
    	return $this->belongsTo('App\Model\Natures');
    }

     public function etat()
    {
    	return $this->belongsTo('App\Model\Etats');
    }

    public function type()
    {
    	return $this->belongsTo('App\Model\Types');
    }

    public function priorite()
    {
    	return $this->belongsTo('App\Model\Priorites');
    }

    public function chrono()
    {
    	return $this->belongsTo('App\Model\Chronos');
    }
}

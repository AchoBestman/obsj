<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EtatCourrier extends Model
{
    protected $guarded = ['id'];
}

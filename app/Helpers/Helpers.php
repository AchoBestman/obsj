<?php

namespace App\Helpers;
use App\Model\Courriers;
use App\Model\Affectations;
use App\Model\Piecesjointes;
use App\Model\Users;
use App\Model\Redirection;
use App\Model\Etats;
use App\Model\Reponse;
use App\Model\Roles;
use App\Model\Departements;
use App\Model\Directions;
use App\Model\DirectionsT;
use App\Model\Archivages;

class  Helpers 
{
   
   public  function __construct()
   {
       
   }

   public static function delaitraitement($debut, $fin)
   {
        $fin = strtotime($fin);
        $debut= strtotime($debut);
        $diff = abs($fin - $debut); 
        $retour = array();
     
        $tmp = $diff;
        $retour['second'] = $tmp % 60;
     
        $tmp = floor( ($tmp - $retour['second']) /60 );
        $retour['minute'] = $tmp % 60;
     
        $tmp = floor( ($tmp - $retour['minute'])/60 );
        $retour['hour'] = $tmp % 24;
     
        $tmp = floor( ($tmp - $retour['hour'])  /24 );
        $retour['day'] = $tmp;
     
        return $retour['day'].' J:'.$retour['hour'].' H:'.$retour['minute'].' M';
   }

   public static function delaitraitementFini($delaiTraitementF)
   {

        /*
        $fin = strtotime($fin);
        $debut= strtotime($debut);
        $todays = strtotime($todays);
        $diffFin = abs($todays - $debut);
        $diff = abs($fin - $debut); 
        $retour = array();
        
        $tempsfin1 = $fin - $debut;
        $tempsfin2 = $todays - $debut;
        $tempsfin = $tempsfin1 - $tempsfin2;

        $tmp = $diff;
        $tmpFin = $diffFin;
        $retour['second'] = $tmpFin % 60;
        $retour['secondFin'] = $tmpFin % 60;
     
        $tmp = floor( ($tmp - $retour['second']) /60 );
        $retour['minute'] = $tmp % 60;
        $tmpFin = floor( ($tmpFin - $retour['secondFin']) /60 );
        $retour['minuteFin'] = $tmpFin % 60;
     
        $tmp = floor( ($tmp - $retour['minute'])/60 );
        $retour['hour'] = $tmp % 24;

        $tmpFin = floor( ($tmpFin - $retour['minuteFin'])/60 );
        $retour['hourFin'] = $tmpFin % 24;
     
        $tmp = floor( ($tmp - $retour['hour'])  /24 );
        $retour['day'] = $tmp;

        $tmpFin = floor( ($tmpFin - $retour['hourFin'])  /24 );
        $retour['dayFin'] = $tmpFin;


        $secon = $retour['secondFin'] - $retour['second'];
        $minu = $retour['minuteFin'] - $retour['minute'];
        $heur = $retour['hourFin'] - $retour['hour'];
        $jour = $retour['dayFin'] - $retour['day'];
        */

        $todays = date('Y-m-d H:i:s');
        $todays = strtotime($todays);
        $delaiTraitementF = strtotime($delaiTraitementF);

        if ($todays >= $delaiTraitementF) {
          return 'Fin de traitement';
        }
        else{

        $fin = $delaiTraitementF;
        $debut= $todays;
        $diff = abs($fin - $debut); 
        $retour = array();
     
        $tmp = $diff;
        $retour['second'] = $tmp % 60;
     
        $tmp = floor( ($tmp - $retour['second']) /60 );
        $retour['minute'] = $tmp % 60;
     
        $tmp = floor( ($tmp - $retour['minute'])/60 );
        $retour['hour'] = $tmp % 24;
     
        $tmp = floor( ($tmp - $retour['hour'])  /24 );
        $retour['day'] = $tmp;
     
        return $retour['day'].' J:'.$retour['hour'].' H:'.$retour['minute'].' M';
        }
     
        
   }


    public static function reference_courrier($id)
   {
        $reference_courrier = Courriers::where('id', $id)->first();
        return $reference_courrier->reference; 
   }

 public static function objet_courrier($id)
   {
        $objet_courrier = Courriers::where('id', $id)->first();
        return $objet_courrier->objet; 
   }

   public static function courrierçid($id)
   {
        $courrierçid = Affectations::where('id', $id)->first();
        return $courrierçid->courrier_id; 
   }


    

      public static function fichiers()
   {
        $xmlFilename = 'whatevernameyouneedhere.xml';

        $xml_tmu = simplexml_load_file($url) or die("Error: Cannot create object");

       Storage::put($xmlFilename, $xml_tmu);
   }

   public static function pieces_jointes($courriers){
    $all = Piecesjointes::where('courrier_id',$courriers)->get();
    return $all;
   }

   public static function pieces_jointes1($courriers,$affectation_id){
    $all = Piecesjointes::where('courrier_id',$courriers)->orWhere('affectation_id',$affectation_id)->get();
    return $all;
   }

   public static function pieces_jointes2($reponse_id){
    $all = Piecesjointes::where('reponse_id',$reponse_id)->get();
    return $all;
   }

   public static function user($user){
      return Users::where('id',$user)->first();
   }

    public static function aff($id){
      $aff_id = Affectations::where('id',$id)->first();
      return $aff_id->etat_id;
   }

   public static function etat($etat){
       $etat = Etats::where('id',$etat)->first();
       if($etat){
        return $etat->libelle;
      }else{
        return '';
      }
       
   }

   public static function courid($numero){
       $courid = Courriers::where('numeroEnregistrement',$numero)->first();
       if($courid){
        return $courid->id;
      }else{
        return '';
      }
       
   }

   public static function reponse($affectations){
      return Reponse::where('affectation_id',$affectations)->first();
   }

   public static function role($role_id){
       $userdd = Roles::where('id', $role_id)->first();
       if ($userdd) {
         return  $userdd->abrev;
       }else{
         return  '';
       }
      

   }
     public static function departement($departement_id){
       $departement = Departements::where('id', $departement_id)->first();
      if ($departement) {
        return $departement->libelle;
      }else{
       return '';
      }

   }

   public static function departement_id($departement_id){
       $departement = Departements::where('id', $departement_id)->first();
       if ($departement) {
        return $departement->id;
      }else{
       return '';
      }
      

   }

 


public static function directiont($direction_id){
       $direction = DirectionsT::where('id', $direction_id)->first();
        if ($direction) {
        return $direction->libelle;
      }else{
       return '';
      }
      

   }

   public static function directiont_id($direction_id){
       $direction = DirectionsT::where('id', $direction_id)->first();
       if ($direction) {
       return $direction->id;
      }else{
       return '';
      }
       

   }

   public static function direction($direction_id){
       $direction = Directions::where('id', $direction_id)->first();
        if ($direction) {
      return $direction->libelle;
      }else{
       return '';
      }
       
       

   }

   public static function affectation_total_admin($me){
    $count = Affectations::all()->count();
       return $count;

   }

public static function archi($id){
    $typearchi = Archivages::where('typearchi', $id)->first();
       if ($typearchi = 1) {
        return 'Semi-archivé';
       }elseif ($typearchi = 2) {
        return 'archivé définitivement';
       }else{
        return 'Remise en ligne';
       }

   }

   public static function affectation_total_me($me)
   {
       $count = Affectations::where('qui', $me)->orWhere('aqui',$me)->get()->count();
       return $count;
   }

    public static function reaffectation_total_me($me)
   {
       $count = Redirection::where('qui', $me)->orWhere('aqui',$me)->get()->count();
       return $count;
   }

 public static function affectation_total_admin_traite($me){
    $all = Affectations::with('etat')->get();
    $table = array();
    foreach ($all as $key => $value) {
      if ($value->etat && $value->etat->libelle == 'Traité') {
        $table [] = $value;
      }
    }
       return count($table);

   }

   public static function affectation_total_me_traite($me)
   {
       $all = Affectations::with('etat')->where('qui', $me)->orWhere('aqui',$me)->get();
       $table = array();
      foreach ($all as $key => $value) {
        if ($value->etat && $value->etat->libelle == 'Traité') {
          $table [] = $value;
        }
      }
       return count($table);
   }

   public static function affectation_total_admin_entraite($me){
    $all = Affectations::with('etat')->get();
    $table = array();
    foreach ($all as $key => $value) {
      if ($value->etat && $value->etat->libelle == 'En cours') {
        $table [] = $value;
      }
    }
       return count($table);

   }

   public static function affectation_total_me_entraite($me)
   {
       $all = Affectations::with('etat')->where('qui', $me)->orWhere('aqui',$me)->get();
       $table = array();
      foreach ($all as $key => $value) {
        if ($value->etat && $value->etat->libelle == 'En cours') {
          $table [] = $value;
        }
      }
       return count($table);
   }

    public static function affectation_total_admin_close($me){
    $all = Affectations::with('etat')->get();
    $table = array();
    foreach ($all as $key => $value) {
      if ($value->etat && $value->etat->libelle == 'Close') {
        $table [] = $value;
      }
    }
       return count($table);

   }

   public static function affectation_total_me_close($me)
   {
       $all = Affectations::with('etat')->where('qui', $me)->orWhere('aqui',$me)->get();
       $table = array();
      foreach ($all as $key => $value) {
        if ($value->etat && $value->etat->libelle == 'Close') {
          $table [] = $value;
        }
      }
       return count($table);
   }


}




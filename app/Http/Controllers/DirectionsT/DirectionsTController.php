<?php

namespace App\Http\Controllers\DirectionsT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\DirectionsT;
use App\Model\Directions;
use App\Model\Departements;
use Illuminate\Support\Facades\Validator;

class DirectionsTController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
          $directionst = DirectionsT::get();
          $directions = Directions::get();
           return view('directiont.index', compact('directionst','directions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:directions']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        $direction_id   = $request->direction_id;
        $this->validator($request->all())->validate();
        DirectionsT::create(['libelle' => $libelle, 'abrev'=>$abrev, 'direction_id'=>$direction_id]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $directionst = DirectionsT::get();
        return view('directiont.index', compact('directionst'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $directionst = DirectionsT::where('id', $id)->get();
         $directions = Directions::get();
        return view('directiont.edit', compact('directionst','directions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        DirectionsT::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev, 'direction_id'=>$direction_id]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->route('DirectionT.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departements = Departements::where('direction_id',$id)->get()->count();
        if ($departements < 1) {
            Directions::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Cette direction est associée à des services.', '');
        }
         
        return redirect()->route('DirectionT.index');
        
    }
}

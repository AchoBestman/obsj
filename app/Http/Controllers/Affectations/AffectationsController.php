<?php

namespace App\Http\Controllers\Affectations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Affectations;
use App\Model\Courriers;
use App\Model\Users;
use App\Model\Roles;
use App\Model\Etats;
use App\Model\Piecesjointes;
use App\Model\Redirection;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Mail;
use App\Mail\AffectionsMail;
use App\Mail\AffectionsMail2;
use App\Helpers\Helpers;
class AffectationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    } 

    
    public function index()
    {
          $me = auth()->user()->id;
          $redirections = array();
          $role_id = auth()->user()->role_id;
          $role = $this->role($role_id);
          $affectations = Affectations::get();        
          $redirections = Redirection::get();
          if ($role == 'sadmin' OR $role == 'admin') {
             $affectations = Affectations::get();        
             $redirections = Redirection::get();
          }else{
             $affectations = Affectations::where('qui', $me)->orWhere('aqui',$me)->get();        
             $redirections = Redirection::with('affectation')->where('qui', $me)->orWhere('aqui',$me)->get();
          }
         
          $etats = Etats::get();
          return view('affectation.index', compact('affectations','redirections','etats'));

    } 

    public  function role($role_id){
       $user = Roles::where('id', $role_id)->first();
       return  $user->abreviation;

   }

    
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function file_existe($filename){
        $file = Piecesjointes::where('libelle',$filename)->get()->count();
        if ($file) {
            return $file;
        }else{
            return '';
        }
    }

    public function store(Request $request)
    {
                $todays = date('Y-m-d H:i:s');
            $new_inst = '';
                 if (isset($request->newinst) && !empty($request->newinst)) {
                $newinst = $request->newinst;
                $new_inst = Affectations::create(['inst' => $newinst]);
                    }
              foreach ($request['aqui'] as $key => $value) {
                 
                $courrier_id = $request->courrier_id;
                $qui   = $request->qui;
                $aqui   =  $value;
                $note   = $request->instructions;
                $instc   = $request->inst;
                $etat_id = $request->etat_id;
                $Jours = $request->Jours;
                $Heures = $request->Heures;
                
                 if (!empty($newinst)) {
                       $inst  = $newinst;
                    }
                    if (empty($newinst)) {
                       $inst  = $instc;
                    }

                    if (empty($inst)) {
                       $inst  = $instc;
                    }
                    
                $Secondes = $request->Minuites * 60;
                if (empty($Jours)) {
                    $Jours = 0;
                }
                if (empty($Heures)) {
                    $Heures = 0;
                }

                $delaiTraitementF = date('Y-m-d H:i:s',strtotime(" $todays + $Jours days $Heures hours $Secondes seconds"));
                $delaiTraitementD = $Jours.'J:'.$Heures.'H:'.$request->Minuites.'M';
                //echo("todays:$todays  ; Jours:$Jours  ;  Heures:$Heures;  Secondes : $Secondes Endate : $enddate");
                //dd(12);
                $affectations = Affectations::where('courrier_id', $courrier_id)
                                             ->where('qui',$qui)
                                             ->where('aqui',$aqui)
                                             ->first();
            if ($affectations) { 
                
                flashy()->success('Courrier deja affecté ou transmit à la personne.', '');
            }
            else{
            $items=   Affectations::create(['courrier_id' => $courrier_id, 'qui'=>$qui, 'aqui'=>$aqui,'delaiTraitementF' => $delaiTraitementF,'delaiTraitementD' => $delaiTraitementD,'intructions' => $note,'etat_id' => $etat_id, 'inst' => $inst ]);

            if($request->hasFile('images'))
            {
            $allowedfileExtension=['pdf','jpg','png','docx','xls','PNG'];
            $files = $request->file('images');

                if ($request->file('images')) { 
                foreach ($request->images as $image) {
                $filename = $image->getClientOriginalName(); // file name
                $extension = $image->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                if ($check) {
                    $image->move(base_path('public\images'), $filename); // uploading file to given path
                    Piecesjointes::create([
                'affectation_id' => $items->id,
                'libelle' => $filename,
                'courrier_id' => $request->courrier_id,
                 
                ]);
                }
               
                
            } 

            }




            }
             foreach ($request['aqui'] as $key => $value) {
            $idm = $value; 
            $email = Users::where('id', $idm)->first();
            
            if (!empty($email)) {
                if (isset($email->email) && !empty($email->email)) {
                    $mail = $email->email;
                    if (isset($request->Affecter) && !empty($request->Affecter)) {
                        Mail::to($mail)->send(new AffectionsMail($delaiTraitementF));
                    }
                    if (isset($request->Transmettre) && !empty($request->Transmettre)) {
                        Mail::to($mail)->send(new AffectionsMail2());
                    }
                    
                }
            }
            
              }
            flashy()->success('Action traitée avec succes.', '');
            }

            
             }
                
           return back();
                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
          $affectations = Courriers::where('id', $id)->get();
          $users = Users::where('id','!=', auth()->user()->id)->get();
          $etats = Etats::get();
          $insts = Affectations::get();
          return view('affectation.edit', compact('affectations', 'users','id','etats', 'insts'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $affectations = Affectations::where('id', $id)->get();
        $users = Users::get();
        $etats = Etats::get();
        return view('redirection.index', compact('affectations', 'users','etats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                 foreach ($request['aqui'] as $key => $value) {
                $courrier_id = $request->courrier_id;
                $qui   = $request->qui;
                $aqui   =  $value;
                
                Redirections::create(['affectation_id' => $courrier_id, 'expediteur'=>$qui, 'destinataire'=>$aqui]);

                    }
                flashy()->success('Action traitée avec succes.', '');
                return redirect()->route('Affectation.index');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $Courriers = Redirection::where('affectation_id',$id)->get()->count();
        if ($Courriers < 1) {
            Affectations::where('id', $id)->delete();
            Redirection::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Ce courrier est associé à d\'autres Courriers.', '');
        }
        return redirect()->route('Affectation.index');
    }
}

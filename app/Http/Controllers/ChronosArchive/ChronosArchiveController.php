<?php

namespace App\Http\Controllers\ChronosArchive;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ChronosA;
use App\Model\Archivages;
use App\Model\Courriers;

class ChronosArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
       $chronos = ChronosA::get();
       return view('chronoA.index', compact('chronos'));
    }

     public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:chronos']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev = $request->abrev;
       // $this->validator($request->all())->validate();
        ChronosA::create(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $courriers = Archivages::where('chrono_a_id', $id)->get();
       // $toto = ChronosA::where('chrono_a_id', $id)->get();
        return view('chronoA.dossier', compact('courriers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $chronos = ChronosA::where('id', $id)->get();
        return view('chrono.edit', compact('chronos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        ChronosA::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
         return redirect()->back();
    }



}
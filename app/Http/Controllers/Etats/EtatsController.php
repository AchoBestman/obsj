<?php

namespace App\Http\Controllers\Etats;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Etats;
use Illuminate\Support\Facades\Validator;
use App\Model\Affectations;
use App\Model\Courriers;
use App\Model\Redirection;
class EtatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $etats = Etats::get();
        return view('etat.index', compact('etats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:etats']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev = $request->abrev;
        $this->validator($request->all())->validate();
        Etats::create(['libelle' => $libelle, 'abrev' => $abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $etats = Priorites::get();
        return view('etat.index', compact('etats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $etats = Etats::where('id', $id)->get();
        return view('etat.edit', compact('etats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        Etats::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->route('Etat.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courriers = Courriers::where('etat_id',$id)->get()->count();
        $affectations = Affectations::where('etat_id',$id)->get()->count();
        $redirctions = Redirection::where('etat_id',$id)->get()->count();
        if ($affectations < 1 || $courriers < 1 || $redirctions <1 ) {
            Etats::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Cet état est associé à des Courriers.', '');
        }
        return redirect()->route('Etat.index');
    }
}

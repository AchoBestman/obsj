<?php

namespace App\Http\Controllers\Redirection;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Affectations;
use App\Model\Users;
use App\Model\Redirection;
use App\Model\Courriers;
use App\Model\Piecesjointes;
use Illuminate\Support\Facades\Mail;
use App\Mail\AffectionsMail;
use App\Mail\AffectionsMail1;

class RedirectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $affectations = Affectations::get();
        $users = Users::get();
        return view('redirection.index', compact('affectations', 'users'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    			foreach ($request['aqui'] as $key => $value) {
                $courrier_id = $request->affectation_id;
                $qui   = $request->qui;
                $aqui   =  $value;
                $instruction   = $request->instruction;
                Redirection::create(['affectation_id' => $courrier_id, 'qui'=>$qui, 'aqui'=>$aqui,  'instructions'=>$instruction]);
                 foreach ($request['aqui'] as $key => $value) {
                    $idm = $value; 
                    $email = Users::where('id', $idm)->first();
                    $mail = $email->email;
                    Mail::to($mail)->send(new AffectionsMail1());
                      }
                flashy()->success('Action traitée avec succes.', '');
               return redirect()->route('Affectation.index');
     
    }


}

/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $affectations = Affectations::where('id', $id)->get();
        $redirections = Redirection::get();
        $users = Users::get();
        return view('redirection.edit', compact('affectations', 'users', 'redirections'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $affectations = Affectations::where('id', $id)->get();
          $courriers = Courriers::get();
          $users = Users::get();
          $pieces = Piecesjointes::get();
          return view('redirection.index', compact('affectations', 'courriers', 'users'));

    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Redirection::where('id', $id)->delete();
         flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }
}

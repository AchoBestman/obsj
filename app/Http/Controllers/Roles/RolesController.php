<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Roles;
use App\Model\Users;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
       $roles = Roles::get();
       return view('role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:roles']
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev = $request->abrev;
        $this->validator($request->all())->validate();
        Roles::create(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Roles::get();
        return view('role.index', compact('roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Roles::where('id', $id)->get();
        return view('role.edit', compact('roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        Roles::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->route('Role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = Users::where('role_id',$id)->get()->count();
        if ($users < 1) {
            Roles::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Ce rôle est associé à des agents.', '');
        }
        return redirect()->route('Role.index');
    }
}

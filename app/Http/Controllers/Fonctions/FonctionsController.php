<?php

namespace App\Http\Controllers\Fonctions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Fonctions;
use App\Model\Users;
use Illuminate\Support\Facades\Validator;

class FonctionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
       $fonctions = Fonctions::get();
       return view('fonction.index', compact('fonctions'));
   }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:fonctions']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        $this->validator($request->all())->validate();
        Fonctions::create(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fonctions = Fonctions::get();
        return view('fonction.index', compact('fonctions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fonctions = Fonctions::where('id', $id)->get();
        return view('fonction.edit', compact('fonctions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        Fonctions::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->route('Fonction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = Users::where('fonction_id',$id)->get()->count();
        if ($users < 1) {
            Fonctions::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Cette fonction est associée à des agents.', '');
        }
        return redirect()->route('Fonction.index');
    }
}

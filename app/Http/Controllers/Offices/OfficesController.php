<?php

namespace App\Http\Controllers\Offices;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Offices;
use App\Model\Departements;
use App\Model\Users;
use Illuminate\Support\Facades\Validator;

class OfficesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
      $offices = Offices::with(['departement'])->get();
      $departements = Departements::get();
      return view('office.index', compact('offices', 'departements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:offices']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        $departement_id   = $request->departement_id;
        $this->validator($request->all())->validate();
        Offices::create(['libelle' => $libelle, 'abrev'=>$abrev, 'departement_id'=>$departement_id]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offices = Offices::get();
        $departements = Departements::get();
        return view('office.index', compact('offices', 'departements'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offices = Offices::where('id', $id)->get();
        $departements = Departements::get();
        return view('office.edit', compact('offices', 'departements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        $departement_id = $request->departement_id;
        Offices::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev, 'departement_id'=>$departement_id]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->route('Office.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = Users::where('office_id',$id)->get()->count();
        if ($users < 1) {
            Offices::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Ce bureau est associée à des agents.', '');
        }
         return redirect()->route('Office.index');
    }
}
 
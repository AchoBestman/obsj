<?php

namespace App\Http\Controllers\Reponse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Courriers;
use App\Model\Users;
use App\Model\Etats;
use App\Model\Affectations;
use App\Model\Reponse;
use App\Model\Piecesjointes;
use App\Model\EtatCourrier;
use App\Model\Redirection;
use Intervention\Image\ImageManagerStatic as Image;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReponseMail;

class ReponseController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $affectations = Affectations::get();
        $courriers = Courriers::get();
        $users = Users::get();
        $etats = Etats::get();
        return view('reponse.index', compact('affectations', 'courriers', 'users','etats'));
    } 

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $affectation = Affectations::where('id', $id)->first();
        $etats = Etats::get();
        return view('reponse.index', compact('affectation','etats'));
}

/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $affectations = Affectations::where('id', $id)->get();
        $courriers = Courriers::get();
        $users = Users::get();
        return view('reponse.index', compact('affectations', 'courriers', 'users'));
    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 

       private function file_existe($filename){
        $file = Piecesjointes::where('libelle',$filename)->get()->count();
        if ($file) {
            return $file;
        }else{
            return '';
        }
    }

    private function affectation($affectation_id){
        $affectation = Affectations::where('id',$affectation_id)->first();
        if ($affectation) {
            return $affectation->qui;
        }else{
            return '';
        }
    }

    private function affectationaqui($affectation_id)
    {
        $affectation = Affectations::where('id',$affectation_id)->first();
        if ($affectation) {
            return $affectation->aqui;
        }else{
            return '';
        }
    }

    private function user_email($qui){
        $users = Users::where('id',$qui)->first();
        if ($users) {
            return $users->email;
        }else{
            return '';
        }
    }

 
    public function store(Request $request)
    {
        $rep = Reponse::where('affectation_id',$request->affectation_id)->first();
        $qui = $this->affectation($request->affectation_id);
        $aqui = $this->affectationaqui($request->affectation_id);
        $emailqui = $this->user_email($qui);
        $emailaqui = $this->user_email($aqui);
        $me = auth()->user()->email;
        //dd("Me: $me;  emailqui:$emailqui;  emailaqui:$emailaqui");
        if ($me == $emailqui) {
            $email = $emailaqui;
        }
        if ($me == $emailaqui) {
            $email = $emailqui;
        }
        if(isset($request->etat_id) && !empty($request->etat_id)){
            EtatCourrier::create([
                'etat_id' => $request->etat_id,
                'affectation_id' => $request->affectation_id,
                ]);
            Affectations::where('id',$request->affectation_id)->update([
                'etat_id' => $request->etat_id,
                ]);
            Redirection::where('affectation_id',$request->affectation_id)->update([
                'etat_id' => $request->etat_id,
                ]);


        }
        if(is_null($rep)){

              $reponse = Reponse::create([
                'affectation_id' => $request->affectation_id,
                'libelle' => $request->libelle,
                ]);
                $reponse_id = $reponse->id;
               Mail::to($email)->send(new ReponseMail($request->libelle, $reponse_id));

           }else{

              $reponse = Reponse::where('affectation_id',$request->affectation_id)->update([
                'libelle' => $request->libelle,
                ]);
              $reponse_id = $rep->id;
              //Mail::to($email)->send(new ReponseMail($request->libelle));
           }
     

            $allowedfileExtension=['pdf','jpg','png','docx','PNG'];

            if(isset($reponse) && !empty($reponse) && $request->file('images')){
            $files = $request->file('images');

                if ($request->file('images')) { 
                foreach ($request->images as $image) {
                $filename = $image->getClientOriginalName(); // file name
                $extension = $image->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                if ($check) {
                    $image->move(base_path('public\images'), $filename); // uploading file to given path
                    Piecesjointes::create(['reponse_id'=> $reponse_id ,'libelle' =>$filename]);
                }
               
                
            } 

            }

                }
               // Mail::to($email)->send(new ReponseMail($request->libelle,$reponse_id ));
                flashy()->success('Action traitée avec succes.', '');
                return redirect()->route('Affectation.index');
     
    }


            public function reponsePdf($id)
                 {
                     $id = 8;
                     $reponse= Reponse::findOrFail($id);
                     $pdf = PDF::loadView('pdf.index', compact('reponse'));
                     $name = "Réponse".$reponse->id.".pdf";
                     return $pdf->download($name);
                 }

}

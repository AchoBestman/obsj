<?php

namespace App\Http\Controllers\Chronos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Chronos;
use App\Model\Courriers;
use Illuminate\Support\Facades\Validator;
use App\Model\Affectations;
use App\Model\Redirection;
class ChronosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
       $chronos = Chronos::get();
       return view('chrono.index', compact('chronos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:chronos']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev = $request->abrev;
        $this->validator($request->all())->validate();
        Chronos::create(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chronos = Chronos::get();
        return view('chrono.index', compact('chronos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $chronos = Chronos::where('id', $id)->get();
        return view('chrono.edit', compact('chronos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        Chronos::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->route('Chrono.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $courriers = Courriers::where('chrono_id',$id)->get()->count();
        $affectations = Affectations::where('chrono_id',$id)->get()->count();
        $redirctions = Redirection::where('chrono_id',$id)->get()->count();
        if ($affectations < 1 && $courriers < 1 && $redirctions <1 ) {
            Chronos::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Cet chrono est associé à des Courriers.', '');
        }
        return redirect()->route('Chrono.index');
    }
}
 
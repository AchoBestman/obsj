<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Users;
use App\Model\Fonctions;
use App\Model\Offices;
use App\Model\Roles;
use Carbon\Carbon;
use App\Model\Affectations;
use App\Model\Courriers;
use App\Model\Redirection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\UsersMail;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    

    
    public function index()
    {
       $users = Users::with(['role', 'fonction', 'office'])->get();
       $fonctions = Fonctions::get();
       $offices = Offices::get();
       $roles = Roles::get();
       return view('user.index', compact('users', 'fonctions', 'offices', 'roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getresetpassword(){
        return view('auth.getresetpassword');
    }

    public function postresetpassword(Request $request){
        $this->validatorpassword($request->all())->validate();
        $u = Users::where('email', $request->email)->first();
        if (isset($u) && isset($u->email) && !empty($u->email)) {
            $passwordnc   = $request->password;
            $password   = bcrypt($request->password);
            Users::where('email', $request->email)
                    ->update(['passwordnc' => $passwordnc,'password' => $password]);
            flashy()->success('Action traitée avec succes.', '');
            return redirect()->route('login');
        }
        else{
            flashy()->error('Adresse email inexistante', '');
            return redirect()->back();
        } 
    }

    protected function validatorpassword(array $data)
    {
        return Validator::make($data, [
        'email' => ['required', 'email', 'max:255'],
        'password' => ['confirmed']
        ]);
    } 



    protected function validator(array $data)
    {
        return Validator::make($data, [
        'email' => ['required', 'email', 'max:255', 'unique:users'],
        'password' => ['confirmed'],
        'nom' => ['required'],
        'prenom' => ['required'],
        'fonction_id' => ['required','integer'],
        'office_id' => ['required','integer'],
        'role_id' => ['required','integer'],
        ]);
    } 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nom = $request->nom;
        $prenom   = $request->prenom;
        $adresse   = $request->adresse;
        $dateNaissance = date('2020-06-15');
        if ($request->dateNaissance !== null) {
             $dateNaissance = $request->dateNaissance;
        }
       
        $lieuNaissance   = $request->lieuNaissance;
        $sexe   = $request->sexe;
        $email   = $request->email;
        $telephone   = $request->telephone;
        $passwordnc   = $request->password;
        $password   = bcrypt($request->password);
        $fonction_id = $request->fonction_id;
        $role_id = $request->role_id;
        $office_id = $request->office_id;
        $this->validator($request->all())->validate();
       $toto = Users::create(['nom' => $nom, 'prenom'=>$prenom, 'adresse'=>$adresse, 'dateNaissance'=>$dateNaissance, 'lieuNaissance'=>$lieuNaissance, 'sexe'=>$sexe, 'telephone'=>$telephone, 'email'=>$email, 'passwordnc'=>$passwordnc, 'password'=>$password, 'fonction_id'=>$fonction_id,'office_id'=>$office_id, 'role_id'=>$role_id]);
         Mail::to($request->email)->send(new UsersMail($request->password));
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = Users::where('id', $id)->get();
        $fonctions = Fonctions::get();
        $offices = Offices::get();
        $roles = Roles::get();
        return view('user.profile', compact('users', 'fonctions', 'offices', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = Users::where('id', $id)->get();
        $fonctions = Fonctions::get();
        $offices = Offices::get();
        $roles = Roles::get();
        return view('user.edit', compact('users', 'fonctions', 'offices', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function update(Request $request, $id)
    {
        $user = Users::where('id', $id)->first();
        $role_id = $user->role_id;
        $office_id = $user->office_id;
        $fonction_id = $user->fonction_id;

        $nom = $request->nom;
        $prenom   = $request->prenom;
        $adresse   = $request->adresse;
        $dateNaissance   = $request->dateNaissance;
        $lieuNaissance   = $request->lieuNaissance;
        $sexe   = $request->sexe;
        $email   = $request->email;
        $telephone   = $request->telephone;
        $passwordnc   = $request->password;
        $password   = bcrypt($request->passwordnc);

        if (!empty($request->fonction_id)) {
           $fonction_id = $request->fonction_id;
        }
        if (!empty($request->role_id)) {
           $role_id = $request->role_id;
        }
        if (!empty($request->office_id)) {
           $office_id = $request->office_id;
        }
        $this->validatorpassword($request->all())->validate();
         Users::where('id', $id)->update(['nom' => $nom, 'prenom'=>$prenom, 'adresse'=>$adresse, 'dateNaissance'=>$dateNaissance, 'lieuNaissance'=>$lieuNaissance, 'sexe'=>$sexe, 'telephone'=>$telephone, 'email'=>$email,'fonction_id'=>$fonction_id,'office_id'=>$office_id, 'role_id'=>$role_id, 'password' => $password,'passwordnc' => $passwordnc]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $affectations = Affectations::where('qui',$id)->orWhere('aqui',$id)->get()->count();
       $courriers = Courriers::where('user_id',$id)->orWhere('expediteur_id',$id)
                                ->orWhere('destinataire_id',$id)->count();
       $redirctions = Redirection::where('expediteur',$id)->orWhere('destinataire',$id)->get()->count();

        if ($affectations < 1 && $courriers < 1 && $redirctions <1 ) {
            Users::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Ce agent est associé à des courriers.', '');
        }
        return redirect()->route('User.index');
    }

    public function deconnexion(){
        auth()->logout();
        return redirect('/');
    }


     public function statut(Request $request, $id)
    {

        $id    = $request->id;
        $statut  = $request->statut;
        //$restaurer = $request->restaurer;
        //$date  = date('Y-m-d H:i:s');
        //if ($restaurer != '') {
        $u = Users::where('id', $id)->update(['statut' => $statut]);
        flashy()->success('Action traitée avec succes.', '');
       // }
        //else{
       // $u = Courriers::where('id', $id)->update(['archive' => $arch, 'datearchive' => $date]);
        //flashy()->success('Action traitée avec succes.', ''); 
        //}
        
        
        return redirect()->route('User.index');
}
 
}

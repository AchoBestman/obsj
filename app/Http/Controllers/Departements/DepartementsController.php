<?php

namespace App\Http\Controllers\Departements;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Departements;
use App\Model\DirectionsT;
use App\Model\Offices;
use Illuminate\Support\Facades\Validator;

class DepartementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $departements = Departements::with(['direction'])->get();
        $directions = DirectionsT::get();
        return view('departement.index', compact('departements', 'directions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
        'libelle' => ['required', 'string', 'max:255', 'unique:departements']
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        $direction_id   = $request->direction_id;
        $this->validator($request->all())->validate();
        Departements::create(['libelle' => $libelle, 'abrev'=>$abrev, 'direction_id'=>$direction_id]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departements = Departements::get();
        $directions = DirectionsT::get();
        return view('departement.index', compact('departements', 'directions'));
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departements = Departements::where('id', $id)->get();
        $directions = DirectionsT::get();
        return view('departement.edit', compact('departements', 'directions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libelle = $request->libelle;
        $abrev   = $request->abrev;
        $direction_id = $request->direction_id;
        Departements::where('id', $id)->update(['libelle' => $libelle, 'abrev'=>$abrev, 'direction_id'=>$direction_id]);
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->route('Departement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bureaux = Offices::where('departement_id',$id)->get()->count();
        if ($bureaux < 1) {
            Departements::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Ce service est associée à des bureaux.', '');
        }
        return redirect()->route('Departement.index');
       
    }
}

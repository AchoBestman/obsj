<?php

namespace App\Http\Controllers\Courriers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Courriers;
use App\Model\Users;
use App\Model\Chronos;
use App\Model\Types;
use App\Model\Natures;
use App\Model\Priorites;
use App\Model\Etats;
use App\Model\Affectations;
use App\Model\Redirection;
use App\Model\EtatCourrier;
use App\Model\Piecesjointes;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Mail;
use App\Mail\ArchivageMail;
use App\Model\Roles;
use Illuminate\Support\Facades\Storage;
class CourriersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public  function role($role_id){
       $user = Roles::where('id', $role_id)->first();
       return  $user->abrev;

   }

    
 
    
    public function index()
    {    
         $user = auth()->user()->id;
         $redirections = array();
         $role_id = auth()->user()->role_id;

         $role = $this->role($role_id);

         $document = Courriers::with(['user', 'type', 'nature', 'chrono'])
         ->where('user_id',$user)
         ->where('motcles',$role)
         ->get();
          if ($role != 'clt') {
         $courriers = Courriers::with(['user', 'type', 'priorite', 'nature', 'chrono', 'expediteur', 'destinataire', 'etat'])->get();
     }else
      {
         $courriers = Courriers::with(['user', 'type', 'priorite', 'nature', 'chrono', 'expediteur', 'destinataire', 'etat'])->where('user_id',$user)->get();
     }
         $userse = Users::get();
         $types = Types::get();
         $natures = Natures::get();
         $chronos = Chronos::get();
         $priorites = Priorites::get();
         $etats = Etats::get();
         return view('courrier.index', compact('courriers', 'userse', 'types', 'natures', 'chronos', 'priorites','etats','document'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
        'reference' => ['required', 'string', 'max:255', 'unique:courriers']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function file_existe($filename){
        $file = Piecesjointes::where('libelle',$filename)->get()->count();
        if ($file) {
            return $file;
        }else{
            return '';
        }
    }
 
    public function store(Request $request)
    { 
            $user_new_exp = ''; $user_new_des = '';
            $role_id = auth()->user()->role_id;
            $role = Roles::where('id',$role_id)->first();

            if (isset($request->newexpediteur) && !empty($request->newexpediteur)) {
                $newexpediteur = $request->newexpediteur;
                $user_new_exp = Users::create(['nom' => $newexpediteur]);
            }
            if (isset($request->newdestinataire) && !empty($request->newdestinataire)) {
                $newdestinataire = $request->newdestinataire;
                $user_new_des = Users::create(['nom' => $newdestinataire]);
            }

            $allowedfileExtension=['pdf','jpg','png','docx','xls','PNG'];
            $filename = null;
            $courrriern = null;
            $check = null;
            if ($request->file('courrriern')) {
            $files = $request->file('courrriern');
            $filename = $files->getClientOriginalName();
            $extension = $files->getClientOriginalExtension();
            $check=in_array($extension,$allowedfileExtension);
              if($check)
                {
                $file = $request->courrriern;
                $courrriern = $file->getClientOriginalName(); // file name
                $file->move(base_path('public\images'), $courrriern); // uploading file to given path
                }
            }


        $nompr          = $request->nompr;
        $nomeneg        = $request->nomeneg;
        $chrono_id      = $request->chrono_id;
        $objet          = $request->objet;
        $confidentiel   = $request->confidentiel;
        $anotation      = $request->anotation;
        $type_id        = $request->type_id;
        $nature_id      = $request->nature_id;
        $priorite_id    = $request->priorite_id;
        $expediteur_id  = $request->expediteur_id;
        $destinataire_id = $request->destinataire_id;
        $cournumerise   = $request->cournumerise;
        $reference      = $request->reference;
        $motcles         = $request->motcles;
        $etat_id           = $request->etat_id;
        $user_id        = \Auth::user()->id;

        if ($role->abrev == "mp" || $role->abrev == "comp") {
            $motcles = $role->abrev;
        }

        if (!empty($user_new_exp)) {
           $expediteur_id  = $user_new_exp->id;
        }
        if (!empty($user_new_des)) {
            $destinataire_id  = $user_new_des->id;
        }

        if (empty($user_new_exp) && empty($expediteur_id)) {
           $expediteur_id  = $user_id;
        }
        if (empty($user_new_des) && empty($destinataire_id)) {
            $destinataire_id  = $user_id;
        }

        $this->validator($request->all())->validate();
       $courrier = Courriers::create(['numeroPrimaire' => $nompr, 'numeroEnregistrement'=>$nomeneg, 'chrono_id'=>$chrono_id, 'objet'=>$objet, 'confidentiel'=>$confidentiel, 'annotations'=>$anotation, 'type_id'=>$type_id, 'nature_id'=>$nature_id, 'priorite_id'=>$priorite_id, 'destinataire_id'=>$destinataire_id, 'expediteur_id'=>$expediteur_id, 'courriersNumerise'=>$courrriern, 'reference'=>$reference, 'etat_id'=>$etat_id, 'user_id'=>$user_id, 'motcles'=> $motcles]);
         
       if (isset($courrier) && !empty($courrier->id)) {

           EtatCourrier::create(['etat_id'=>$etat_id,'courrier_id'=>$courrier->id]);

            if ($request->file('images')) { 
                foreach ($request->images as $image) {
                $filename = $image->getClientOriginalName(); // file name
                $extension = $image->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                if ($check) {
                    $image->move(base_path('public\images'), $filename); // uploading file to given path
                    Piecesjointes::create(['courrier_id'=> $courrier->id ,'libelle' =>$filename]);
                }
               
                
            } 

            }

            }
 
        flashy()->success('Action traitée avec succes.', '');
        return redirect()->back();
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $courriers = Courriers::with(['user', 'type', 'priorite', 'nature', 'chrono', 'expediteur', 'destinataire', 'etat'])->where('id', $id)->get();
         $users = Users::get();
         $types = Types::get();
         $natures = Natures::get();
         $chronos = Chronos::get();
         $priorites = Priorites::get();
         $etats = Etats::get();
         return view('courrier.consulter', compact('courriers', 'users', 'types', 'natures', 'chronos', 'priorites','etats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $courriers = Courriers::where('id', $id)->get();
         $users = Users::get();
         $types = Types::get();
         $natures = Natures::get();
         $chronos = Chronos::get();
         $priorites = Priorites::get();
         return view('courrier.edit', compact('courriers', 'users', 'types', 'natures', 'chronos', 'priorites'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function update(Request $request, $id)
    {
        $nompr          = $request->nompr;
        $nomeneg        = $request->nomeneg;
        $chrono_id      = $request->chrono_id;
        $objet          = $request->objet;
        $confidentiel   = $request->confidentiel;
        $anotation      = $request->anotation;
        $type_id        = $request->type_id;
        $nature_id      = $request->nature_id;
        $priorite_id    = $request->priorite_id;
        $expediteur_id  = $request->expediteur_id;
        $destinataire_id = $request->destinataire_id;
        $cournumerise   = $request->cournumerise;
        $reference      = $request->reference;
        $user_id        = \Auth::user()->id;
        Courriers::where('id', $id)->update(['numeroPrimaire' => $nompr, 'numeroEnregistrement'=>$nomeneg, 'chrono_id'=>$chrono_id, 'objet'=>$objet, 'confidentiel'=>$confidentiel, 'annotations'=>$anotation, 'type_id'=>$type_id, 'nature_id'=>$nature_id, 'priorite_id'=>$priorite_id, 'destinataire_id'=>$destinataire_id, 'expediteur_id'=>$expediteur_id, 'reference'=>$reference, 'user_id'=>$user_id]);
         flashy()->success('Action traitée avec succes.', '');
         return redirect()->route('Courrier.index');
    }


     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request, $id)
    {

        $id    = $request->id;
        $arch  = $request->archive;
        $restaurer = $request->restaurer;
        $date  = date('Y-m-d H:i:s');
        if ($restaurer != '') {
        $u = Courriers::where('id', $id)->update(['archive' => $arch, 'datearchive' => $date, 'corbeille' => $restaurer]);
        flashy()->success('Action traitée avec succes.', '');
        }
        else{
        $u = Courriers::where('id', $id)->update(['archive' => $arch, 'datearchive' => $date]);
        flashy()->success('Action traitée avec succes.', ''); 
        }
        
        
        return redirect()->route('Courrier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
         $affectations = Affectations::where('courrier_id',$id)->get()->count();
        if ($affectations < 1) {
            //Courriers::where('id', $id)->delete();
          Courriers::where('id', $id)->update(['corbeille'=> 1]);
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Ce courrier est associé à d\'autres Courriers.', '');
        }
        return redirect()->route('Courrier.index');
    }

    public function supprimecourrier($id)
    {
        $affectations = Affectations::where('courrier_id',$id)->get()->count();
        if ($affectations < 1) {
            //Courriers::where('id', $id)->delete();
          Courriers::where('id', $id)->delete();
            flashy()->success('Action traitée avec succes.', '');
        }else{
            flashy()->error('Erreur de suppression.Ce courrier est associé à d\'autres Courriers.', '');
        }
        return redirect()->route('Courrier.index');
    }

    public function close($affect_id, $etat_id, $courrier_id){

        Affectations::where('id',$affect_id)->update(['etat_id'=>$etat_id]);
        Redirection::where('affectation_id',$affect_id)->update(['etat_id'=>$etat_id]);
        EtatCourrier::create([
                'etat_id' => $etat_id,
                'courrier_id' => $affect_id,
                ]);
        $date  = date('Y-m-d H:i:s');
        Courriers::where('id', $courrier_id)->update(['archive' => 1, 'datearchive' => $date]);
        $abrev = "arch";
        $role = Roles::where('abrev', $abrev);
        $idrole = $role->id;
        $user_id = Users::where('role_id', $idrole);
        $ruser = $user_id->id;
        $toto = Users::where('id', $ruser)->first();
        $email = $toto->email;
        Mail::to($email)->send(new ArchivageMail());
        flashy()->success('Action traitée avec succes.', '');
        return back();

    }
} 
 
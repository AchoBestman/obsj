<?php

namespace App\Http\Controllers\Archivages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Courriers;
use App\Model\ChronosA;
use App\Model\Archivages;

class ArchivagesController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $courriers = Courriers::get();
        $chronos = ChronosA::get();
        $archivage = Archivages::get();
        return view('archivage.index', compact('courriers', 'archivage','chronos'));
    }

    public function show($id)
    {
         $courriers = Courriers::where('id', $id)->get();
          $chronos = ChronosA::get();
         return view('archivage.archiver', compact('courriers','chronos'));
    }

     public function store(Request $request)
    {
        $id = $request->idcour;
        $chrono_id = $request->chrono_id;
        $numero = $request->numerocourrier;
        $typearchi   = $request->typearchi;
        $datefinarchi   = $request->datefinarchi;
        $instructions   = $request->instructions;
       // $this->validator($request->all())->validate();
        Archivages::create(['numero' => $numero, 'typearchi'=>$typearchi, 'datefinarchi'=>$datefinarchi, 'chrono_a_id'=>$chrono_id, 'instructions'=>$instructions, 'corbeille'=>0 ]);
        Courriers::where('id',$id)->update(['semiarchive'=>1]);
        flashy()->success('Action traitée avec succes.', '');
         return redirect()->route('Archivage.index');
    }

    public function archivedef($numero){

        Archivages::where('numero',$numero)->update(['typearchi'=>2]);
        
        return back();

    }

     public function delete($id){

        Archivages::where('id',$id)->update(['corbeille'=>1]);
        
        return back();

    }

    public function remettre($id){

        Courriers::where('numeroEnregistrement',$id)->update(['semiarchive'=>0]);
         Archivages::where('numero',$id)->update(['typearchi'=>0]);
        return back();

    }

     public function edit($id){
          $archivages = Archivages::where('id', $id)->get();
         //$courriers = Courriers::get();
          $chronos = ChronosA::get();
         return view('archivage.edit', compact('archivages','chronos'));
        //$id = $request->idcour;
        //Archivages::where('id',$id)->update(['datefinarchi'=>0]);
        
     //   return back();

    }

     public function update(Request $request, $id){

          $datefinarchi = $request->datefinarchi;

        Archivages::where('id',$id)->update(['datefinarchi'=>$datefinarchi]);
        flashy()->success('Action traitée avec succes.', '');
         return redirect()->route('Archivage.index');

    }
}
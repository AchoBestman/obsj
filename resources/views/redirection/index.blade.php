@extends('layouts.app1')
@section('main')
<br><br>
          <div class="container" style="max-width: 1000px;">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                
                
                    <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab" disabled="disabled">Ré-Affecter le courrier</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">


                  <!-- /.tab-pane -->

                  <div class=" active tab-pane" id="settings">

                   <div class="col-md-12 col-sm-12 col-lg-12">
                     
                   
                    <form class="form-horizontal" method="POST" action="{{route('Redirection.store') }}">
                       @csrf
                        @foreach($affectations as $affectation)
                       <input type="hidden" name="affectation_id" value="{{$affectation->id}}">  
                       @endforeach               
                      <input type="hidden" name="qui"  value="{{ \Auth::user()->id }}">
                      <div class="form-group ">
                        <label for="inputName2" >Destinataires(s)</label>
                        <div >
                        <select class="form-control select2 @error('aqui') is-invalid @enderror" name="aqui[]"  required autocomplete="aqui" autofocus multiple="multiple" data-placeholder="Selectionner">
                         @foreach($users as $user)
                            <option value="{{ $user->id }}" >
                              {{ $user->nom }} {{ $user->prenom }}</option>

                         @endforeach
                        </select>
                      </div>
                        
                         @error('aqui')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 

                      <div class="form-group " >
                        <label for="inputName" >Instructions</label>
                        <div>
                          <textarea class="form-control @error('intructions') is-invalid @enderror" id="inputName" placeholder="" name="instruction"  autocomplete="intructions" autofocus></textarea>
                        </div>
                         @error('intructions')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                      </div>  
                          <button type="submit" class="btn btn-success">Ré-affecter</button>
                    </form>
                    </div>
                 
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection


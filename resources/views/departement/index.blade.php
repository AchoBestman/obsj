@extends('layouts.app1')
@section('main')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Liste des Services</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Ajouter</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <!-- /.tab-pane -->
                  <div class="active tab-pane" id="timeline">
                <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Nom</th>
                      <th>Abréviation</th>
                      <th>Direction</th>
                      <th>Actions</th>                     
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nom</th>
                      <th>Abréviation</th>
                      <th>Direction</th>
                      <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($departements as $departement)
                    <tr>
                      <td> {{ $departement->libelle }}</td>
                      <td> {{ $departement->abrev }}</td>                      
                      <td> {{ $departement->direction? $departement->direction->libelle:'' }}</td> 
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('Departement.edit', $departement->id) }}">Modifier</a> 
                          </div> </div></td>            
                    </tr>
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" method="POST" action="{{ route('Departement.store') }}">
                       @csrf
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nom</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control @error('libelle') is-invalid @enderror" id="inputName" placeholder="Nom du service" name="libelle" value="{{ old('libelle') }}" required autocomplete="libelle" autofocus>
                        </div>
                         @error('libelle')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                      </div>                      
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Abréviation</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control @error('abrev') is-invalid @enderror" id="inputName2" placeholder="abréviation" name="abrev" value="{{ old('abrev') }}" required autocomplete="abrev" autofocus>
                        </div>
                         @error('abrev')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>      
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Directions</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('direction_id') is-invalid @enderror" name="direction_id" value="{{ old('direction_id') }}" required autocomplete="direction_id" autofocus>
                          <option>Selectionner</option>
                         @foreach($directions as $direction)
                            <option value="{{ $direction->id }}" >
                              {{ $direction->libelle }}</option>

                         @endforeach
                        </select>
                      </div>
                        
                         @error('direction_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Ajouter</button>
                        </div>
                      </div>
                      </form>
                      </div>                    
                      
                    
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>

@endsection
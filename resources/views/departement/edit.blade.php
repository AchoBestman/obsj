@extends('layouts.app1')
@section('main')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Modification de service</a></li>                 
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">
                @foreach($departements as $departement)
                <form class="form-horizontal" method="POST" action="{{ route('Departement.update', $departement->id) }}">
                       @csrf
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nom</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control @error('libelle') is-invalid @enderror" id="libelle" placeholder="Nom de la direction" name="libelle" value="{{ $departement->libelle }}" required autocomplete="libelle" autofocus>
                        </div>
                         @error('libelle')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                      </div>                      
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Abréviation</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control @error('abrev') is-invalid @enderror" id="inputName2" placeholder="Name" name="abrev" value="{{$departement->abrev }}" required autocomplete="abrev" autofocus>
                        </div>
                         @error('abrev')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                        <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Direction</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('direction_id') is-invalid @enderror" name="direction_id"  required autocomplete="direction_id" autofocus>
                          <option value="{{ $departement->direction_id }}">Selectionner</option>
                         @foreach($directions as $direction)
                            <option value="{{ $direction->id }}" >
                              {{ $direction->libelle }}</option>

                         @endforeach
                        </select>
                      </div>
                        
                         @error('direction_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Modifer</button>
                        </div>
                      </div>
                     {{ method_field('PUT') }}
                    </form>
                    @endforeach
                  </div>
               
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection
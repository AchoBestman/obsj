  @inject('role', 'App\Helpers\Helpers')
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="/templates/dist/img/logo akpakpa.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">OBSVJ-Courriers</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Sidebar user panel (optional) -->
       <br>
       <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon ion ion-person"></i>
              <p>
                {{\Auth::user()->nom?? ''}} {{\Auth::user()->prenom?? ''}}
                
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/profile/{{$id = \Auth::user()->id }}" class="nav-link">
                  <i class="nav-icon far fa-circle text-info"></i>
                  <p>Mon compte</p>
                </a>
              </li>   
              <li class="nav-item">
                <a href="/deconnexion" class="nav-link">
                  <i class="nav-icon far fa-circle text-danger"></i>
                  <p>Se déconnecter</p>
                </a>
              </li>                              
            </ul>
          </li>
          
        </ul></nav>
        <hr style="width: 200px;
    border-block-start-color: #6c757d;">
         
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if($role::role(Auth::user()->role_id) == "comp")
          <li class="nav-item has-treeview">
            <a href="{{ route('Courrier.index') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>Comptabilité</p>
              <i class="right fas fa-angle-left"></i>
            </a>
          </li>
          @endif
            @if($role::role(Auth::user()->role_id) == "mp")
          <li class="nav-item has-treeview">
            <a href="{{ route('Courrier.index') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>Marché public</p>
              <i class="right fas fa-angle-left"></i>
            </a>
          </li>
            @endif
        @if($role::role(Auth::user()->role_id) == "sadmin")
          <li class="nav-item has-treeview ">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Structure
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item has-treeview ">
                <a href="#" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Directions</p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                <a href="{{ route('Direction.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Directions Générales</p>
                </a>                
              </li>
              <li class="nav-item">
                <a href="{{ route('DirectionT.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Directions Techniques</p>
                </a>
              </li>
                </ul>
              </li>
              <li class="nav-item">
                <a href="{{ route('Departement.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Services</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('Office.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bureaux</p>
                </a>
              </li>
             
              <li class="nav-item">
                <a href="{{ route('Fonction.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fonctions</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('Role.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Rôles</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('User.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Personel</p>
                </a>
              </li>
            </ul>
          </li>
         @endif


          @if($role::role(Auth::user()->role_id) == "sadmin" || $role::role(Auth::user()->role_id) == "srt" || $role::role(Auth::user()->role_id) == "comp" || $role::role(Auth::user()->role_id) == "mp")
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
               Données de base 
                <i class="fas fa-angle-left right"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview">
             
              <li class="nav-item">
                <a href="{{ route('Type.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Types</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{ route('Chrono.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Chronos</p>
                </a>
              </li>  
              <li class="nav-item">
                <a href="{{ route('Nature.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Natures</p>
                </a>
              </li>
              @if($role::role(Auth::user()->role_id) == "sadmin" || $role::role(Auth::user()->role_id) == "srt") 
              <li class="nav-item">
                <a href="{{ route('Priorite.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Priorités</p>
                </a>
              </li>  
              
              <li class="nav-item">
                <a href="{{ route('Etat.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>États</p>
                </a>
              </li>  
              @endif                    
            </ul>
          </li>
          @endif


          @if($role::role(Auth::user()->role_id) == "sadmin" || $role::role(Auth::user()->role_id) == "srt")
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
               Options Courriers 
                <i class="fas fa-angle-left right"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{ route('Courrier.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Courriers</p>
                </a>
              </li>                      
            </ul>
          </li>
          @endif



          @if($role::role(Auth::user()->role_id) != "arch" && $role::role(Auth::user()->role_id) != "mp"  && $role::role(Auth::user()->role_id) != "comp")
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                @if($role::role(Auth::user()->role_id) == "sadmin")
                     Transferts & Affectations
                @endif
                @if($role::role(Auth::user()->role_id) == "srt")
                      Transferts
                @endif
                 @if($role::role(Auth::user()->role_id) == "admin")
                      Affectations
                @endif
                @if($role::role(Auth::user()->role_id) == "clt")
                      Affectations
                @endif
                
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('Affectation.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste</p>
                </a>
              </li>                              
            </ul>
          </li>
          @endif
          @if($role::role(Auth::user()->role_id) == "arch")
           <li class="nav-item has-treeview">
            <a href="{{ route('Archivage.index') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>Archivage</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ route('ChronosArchivage.index') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>Dossier</p>
            </a>
          </li>
          @endif

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
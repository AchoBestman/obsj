<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body style="background-color: cornflowerblue;">
    
   
        <main class="py-4">
            @yield('content')
        </main>
    
</body>
</html>
<script src="/templates/plugins/jquery/jquery.min.js"></script>
<script src="/templates/plugins/select2/js/select2.full.min.js"></script>
<script src="/templates/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/templates/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/templates/dist/js/demo.js"></script>
<!-- Bootstrap -->
<script src="/templates/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="/templates/dist/js/adminlte.js"></script>
<script src="/templates/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/templates/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/templates/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/templates/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/dist/js/adminlte.min.js"></script>
<script src="/templates/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/templates/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/templates/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/templates/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="/templates/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="/templates/plugins/chart.js/Chart.min.js"></script>
<script src="/templates/dist/js/demo.js"></script>
<script src="/templates/dist/js/pages/dashboard3.js"></script>
<script src="/templates/dist/js/demo.js"></script>
<!-- Summernote -->
<script src="/templates/plugins/summernote/summernote-bs4.min.js"></script>

@include('flashy::message')
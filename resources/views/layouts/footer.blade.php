 <footer class="main-footer">
  <div class="row">
    <div class="col-md-3">
       <strong>GDOC+ &copy; 2020</strong>
    </div>
    <div class="col-md-6">
       Licence accordée a l'<em style="color: blue;">Office Béninois des Services de Volontariat des Jeunes</em>
    </div>
    <div class="col-md-3">
       <b style="float: right;">GED/SAE</b>
    </div>
  </div>
   
   
   
     
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->


<!-- jQuery -->
<script src="/templates/plugins/jquery/jquery.min.js"></script>
<script src="/templates/plugins/select2/js/select2.full.min.js"></script>
<script src="/templates/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/templates/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/templates/dist/js/demo.js"></script>
<!-- Bootstrap -->
<script src="/templates/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="/templates/dist/js/adminlte.js"></script>
<script src="/templates/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/templates/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/templates/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/templates/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/dist/js/adminlte.min.js"></script>
<script src="/templates/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/templates/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/templates/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/templates/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="/templates/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="/templates/plugins/chart.js/Chart.min.js"></script>
<script src="/templates/dist/js/demo.js"></script>
<script src="/templates/dist/js/pages/dashboard3.js"></script>
<script src="/templates/dist/js/demo.js"></script>
<!-- Summernote -->
<script src="/templates/plugins/summernote/summernote-bs4.min.js"></script>

@include('flashy::message')
   <script>
   $(".select2").select2({
    theme: "bootstrap",
    width: 'auto',
    dropdownAutoWidth: true,
    allowClear: true,
  });
          </script>
<script>
    $(function () {
        //Add text editor
        $('#compose-textarea').summernote()
      })
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });

     $("#example3").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
     $("#example2").DataTable({
      "responsive": true,
      "autoWidth": false,
    });

     $("#example4").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
     $("#example5").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
   
    /*$('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
*/
    
  });
</script> 
<script >
   $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
</script> 

<script >
   $(function () {

    $('#bnewdestinataire').on('click',function(){
      $('#dnewdestinataire').slideToggle(300);
    });

    $('#bnewexpediteur').on('click',function(){
      $('#dnewexpediteur').slideToggle(300);
    });
    



    })
</script> 
<script >
   $(function () {

    $('#bnewinst').on('click',function(){
      $('#dnewinst').slideToggle(300);
    });

    
    



    })
</script> 


<script >
   $(function () {
   //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
     $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )
   })
</script> 


</body>
</html>

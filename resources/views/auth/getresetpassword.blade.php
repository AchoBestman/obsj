<div class="container-fluid" style=" margin-bottom: 50px; background-color: #fff;">
    <div class=" row">
    <div class="col-md-3">
          <img style="width: 150px; height: auto;" src="/templates/dist/img/benin.jpg">
 
    </div>
     <div class="col-md-6">
       <h3 style="text-align: center; margin-top: 50px;">PLATEFORME DE GESTION DES COURRIERS</h3>
       <h6 style="text-align: center;"><em>Office Béninois des Services de Volontariat des Jeunes</em></h6>

    </div>
     <div class="col-md-3">
                  <img  style="width: 150px; height: auto; float: right;" src="/templates/dist/img/logo akpakpa.jpg">

    </div>
    </div> 
</div>

@extends('layouts.app')

@section('content')
<div class="container-fluid">
<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Changer mot de passe') }}
                    <h4 style="float: right;">OBSVJ</h4>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('postresetpassword') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Nouveau Mot de passe') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Confirmation') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Modifier') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@extends('layouts.app1')
@section('main')
@inject('delaitraitement', 'App\Helpers\Helpers')
@inject('reference_courrier', 'App\Helpers\Helpers')
@inject('users', 'App\Helpers\Helpers')
@inject('role', 'App\Helpers\Helpers')
@inject('courriercr', 'App\Helpers\Helpers')
@inject('objet_courrier', 'App\Helpers\Helpers')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 @if($role::role(Auth::user()->role_id) == "sadmin")
                  <li class="nav-item"><a class="nav-link active" href="#transfert" data-toggle="tab">
                  Liste des Transferts 
                  </a></li>
                  @endif
                  @if($role::role(Auth::user()->role_id) != "srt")
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">
                  Liste des Affectations
                  </a></li>
                  <li class="nav-item"><a class="nav-link " href="#timeline1" data-toggle="tab">Liste des Ré-affectations </a></li>
                  @endif
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
 
                  <!-- /.tab-pane -->
                <div class="tab-pane active" id="transfert">
                <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Reference</th>
                      <th>Objet</th>
                      <th>Date de transfert</th>                  
                      <th>Expéditeurs</th>
                      <th>Agent traitant</th>
                      <th>États</th>
                      <th>Actions</th>

                    </tr>
                  </thead>
                  <tfoot> 
                    <tr>
                      <th>Reference</th>
                      <th>Objet</th>
                      <th>Date de transfert</th>                  
                      <th>Expéditeurs</th>
                      <th>Agent traitant</th>
                      <th>États</th>
                      <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($affectations as $affectation)
                    @if($users::etat($affectation->etat_id) == "Transmit")
                    <tr> 
                      <td> {{ $affectation->courrier?$affectation->courrier->reference:'' }}</td>
                      <td>  {{ $affectation->courrier?$affectation->courrier->objet:'' }}</td>
                      <td> {{ $affectation->created_at }}</td>

                      <td> {{$affectation->qui == \Auth::user()->id? 'Moi' : $users::user($affectation->qui)->nom.' '. $users::user($affectation->qui)->prenom}}</td> 
                       <td>  {{$affectation->aqui == \Auth::user()->id? 'Moi' : $users::user($affectation->aqui)->nom.' '. $users::user($affectation->aqui)->prenom}}</td>
                    
                      <td> <p style=" color: ">{{$etat = $users::etat($affectation->etat_id)?? ''}}</p> 
                       </td>
                       <td>
                       <div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button> 
                          <div class="dropdown-menu">
                             <a class="dropdown-item" href="{{ route('Courrier.show', $affectation->courrier_id) }}">Consulter</a>
                            @if($affectation->aqui == \Auth::user()->id)
                              <a class="dropdown-item" href="{{ route('Affectation.show', $affectation->courrier_id) }}">Affecter</a> 
                              @endif
                            @if($etat != 'Close' && $delaitraitement::delaitraitementFini($affectation->delaiTraitementF, $affectation->delaiTraitementD) != 'Fin de traitement' &&  $affectation->qui == \Auth::user()->id && $role::role(Auth::user()->role_id) != "srt")
                            <a class="dropdown-item" href="{{ route('Affectation.show', $affectation->courrier_id) }}">Affecter</a>
                            @endif
                            @if($delaitraitement::delaitraitementFini($affectation->delaiTraitementF, $affectation->delaiTraitementD) != 'Fin de traitement' && $role::role(Auth::user()->role_id) != "srt")
                            <a class="dropdown-item" href="{{ route('Reponse.show', $affectation->id) }}">Traiter</a>
                            @endif
                             @if($affectation->qui == \Auth::user()->id)
                           <form method="POST" action="{{ route('Affectation.destroy', $affectation->id) }}">
                             @csrf
                             {{ method_field('DELETE') }}
                            <button  class="dropdown-item" >Annuler
                           </button>
                          </form> 
                          @endif                        
                           @foreach($etats as $etat)
                          @if($etat->libelle == "Close" &&  $affectation->qui == \Auth::user()->id)                            
                                  <a class="dropdown-item" href="{{ route('close',['affect_id'=>$affectation->id, 'etat_id'=>$etat->id, 'courrier_id'=>$affectation->courrier_id]) }}">Close</a>
                          @endif
                               @endforeach 
                             
                        </div></div></td> 
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>


                  <div class="tab-pane" id="timeline">
                <div class="table-responsive">
                 <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Reference</th>
                      <th>Objet</th>
                      <th>Date d'Affectation</th>
                      <th>Délais de traitement</th> 
                      <th>Délais restant</th>                    
                      <th>Expéditeurs</th>
                      <th>Agent traitant</th>
                      <th>États</th>
                      <th>Actions</th>

                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Reference</th>
                      <th>Objet</th>
                      <th>Date d'Affectation</th>
                      <th>Délais de traitement</th> 
                      <th>Délais restant</th>                    
                      <th>Expéditeurs</th>
                      <th>Agent traitant</th>
                      <th>États</th>
                      <th>Actions</th>

                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($affectations as $affectation)
                    @if($users::etat($affectation->etat_id) != "Transmit")
                    <tr> 
                      <td> {{ $affectation->courrier?$affectation->courrier->reference:'' }}</td>
                      <td>  {{ $affectation->courrier?$affectation->courrier->objet:'' }}</td>
                      <td> {{ $affectation->created_at }}</td>
                     
                      <td>   {{$affectation->delaiTraitementD}}</td>
                      <td>{{$delaiFin = $delaitraitement::delaitraitementFini($affectation->delaiTraitementF)}}</td>
                  
                      <td> {{$affectation->qui == \Auth::user()->id? 'Moi' : $users::user($affectation->qui)->nom.' '. $users::user($affectation->qui)->prenom}}</td> 
                       <td>  {{$affectation->aqui == \Auth::user()->id? 'Moi' : $users::user($affectation->aqui)->nom.' '. $users::user($affectation->aqui)->prenom}}</td>
                    
                      <td> <p style=" color: ">{{$etat = $users::etat($affectation->etat_id)?? ''}}</p> 
                       </td>
                       <td>
                       <div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button> 
                          <div class="dropdown-menu">
                             <a class="dropdown-item" href="{{ route('Courrier.show', $affectation->courrier_id) }}">Consulter</a>
                            @if($affectation->aqui == \Auth::user()->id && $role::role(Auth::user()->role_id) != "clt" && $role::role(Auth::user()->role_id) != "srt")
                              <a class="dropdown-item" href="{{ route('Affectation.show', $affectation->courrier_id) }}">Affecter</a> 
                              @endif
                            @if($etat != 'Close' && $delaiFin != 'Fin de traitement' &&  $affectation->qui == \Auth::user()->id)
                            <a class="dropdown-item" href="{{ route('Redirection.show', $affectation->id) }}">Réaffecter</a>
                            @endif
                            @if($delaiFin != 'Fin de traitement' && $role::role(Auth::user()->role_id) != "srt")
                            <a class="dropdown-item" href="{{ route('Reponse.show', $affectation->id) }}">Traiter</a>
                            @endif
                             @if($affectation->qui == \Auth::user()->id)
                           <form method="POST" action="{{ route('Affectation.destroy', $affectation->id) }}">
                             @csrf
                             {{ method_field('DELETE') }}
                            <button  class="dropdown-item" >Annuler
                           </button>
                          </form> 
                          @endif                        
                           @foreach($etats as $etat)
                          @if($etat->libelle == "Close" &&  $affectation->qui == \Auth::user()->id)                            
                                  <a class="dropdown-item" href="{{ route('close',['affect_id'=>$affectation->id, 'etat_id'=>$etat->id, 'courrier_id'=>$affectation->courrier_id]) }}">Close</a>
                          @endif
                               @endforeach 
                             
                        </div></div></td> 
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>
                  <!-- /.tab-pane -->    

                   <!-- /.tab-pane -->
                  <div class=" tab-pane" id="timeline1">
                <div class="table-responsive">
                 <table id="example3" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                   
                      <th>Référence</th>
                       <th>Objet</th>
                      <th>Date de ré-affectation</th>
                      <th>Délais de traitement</th> 
                      <th>Délais restant</th>                       
                      <th>Expéditeurs</th>
                      <th>Agent traitant</th>
                     
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr> 
                     
                      <th>Référence</th>
                       <th>Objet</th>
                      <th>Date de ré-affectation</th>
                      <th>Délais de traitement</th>      
                      <th>Délais restant</th>                     
                      <th>Expéditeurs</th>
                      <th>Agent traitant</th>
                      <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($redirections as $affectation)

                    <tr> 
                      <td> {{$reference_courrier::reference_courrier($courriercr::courrierçid($affectation->affectation_id))}}</td>
                      <td>  {{$objet_courrier::objet_courrier($courriercr::courrierçid($affectation->affectation_id))}}</td>
                      <td> {{ $affectation->created_at }}</td>
                     
                      <td>   {{$affectation->affectation->delaiTraitementD}}</td>
                      <td>{{$delaiFin = $delaitraitement::delaitraitementFini($affectation->affectation->delaiTraitementF)}}</td>
                  
                      <td> {{$affectation->qui == \Auth::user()->id? 'Moi' : $users::user($affectation->qui)->nom.' '. $users::user($affectation->qui)->prenom}}</td> 
                       <td>  {{$affectation->aqui == \Auth::user()->id? 'Moi' : $users::user($affectation->aqui)->nom.' '. $users::user($affectation->aqui)->prenom}}</td>
                     
                       <td>
                       <div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button> 
                          <div class="dropdown-menu">
                            
                            @if($delaiFin != 'Fin de traitement')
                            <a class="dropdown-item" href="{{ route('Reponse.show', $affectation->affectation_id) }}">Traiter</a>
                            @endif
                            @if($affectation->qui == \Auth::user()->id)
                           <form method="POST" action="{{ route('Affectation.destroy', $affectation->id) }}">
                             @csrf
                             {{ method_field('DELETE') }}
                            <button  class="dropdown-item" >Annuler
                           </button>
                          </form>  
                          @endif                        
                           @foreach($etats as $etat)
                          @if($etat->libelle == "Close" &&  $affectation->qui == \Auth::user()->id)  
                                                   
                                  <a class="dropdown-item" href="{{ route('close',['affect_id'=>$affectation->id, 'etat_id'=>$etat->id, 'courrier_id'=>$courriercr::courrierçid($affectation->affectation_id)]) }}">Close</a>
                                   
                          @endif
                               @endforeach 
                             
                        </div></div></td> 
                    </tr>
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>
                  <!-- /.tab-pane -->                  
                      
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection


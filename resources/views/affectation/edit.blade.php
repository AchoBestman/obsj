@extends('layouts.app1')
@section('main')
@inject('reference_courrier', 'App\Helpers\Helpers')
@inject('role', 'App\Helpers\Helpers')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                
       
                    <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab" disabled="disabled">
                      @if($role::role(Auth::user()->role_id) != "srt")
                      Affecter un courrier
                      @endif
                      @if($role::role(Auth::user()->role_id) == "srt")
                      Transmettre un courrier
                      @endif
                    </a>
                    </li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content"> 

               
                  <div class=" active tab-pane" id="settings">
                    <form class="form-horizontal" method="POST" action="{{ route('Affectation.store') }}" enctype="multipart/form-data" id="formulaire">
                       @csrf
                        <div class="row">
                        <div class="col-md-6">
                     <div class="form-group">
                        <label for="inputName2" >Référence du courrier</label>
                        <div class="col-sm-10">
                        <select  class="form-control select2 @error('courrier_id') is-invalid @enderror" name="courrier_id" required autocomplete="courrier_id" autofocus >
                         
                            <option selected="selected" value="{{$id}}" >
                              {{$reference_courrier::reference_courrier($id)}}</option>

                        </select>
                      </div>
                        
                         @error('courrier_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>             
                       <div class="form-group" hidden="hidden">
                        <label for="inputName2">Expéditeur</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('qui') is-invalid @enderror" name="qui" value="{{ old('qui') }}" required autocomplete="qui" autofocus>
                         @foreach($users as $user)
                            <option value=" {{ \Auth::user()->id }}" >
                              {{ \Auth::user()->nom }} {{ \Auth::user()->prenom }}</option>

                         @endforeach
                        </select>
                      </div>
                        
                         @error('qui')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      <div class="form-group">
                        <label for="inputName2">Pièces jointes</label>
                        <div class="col-sm-10">
                               <input type="file" id="image" class="form-control" value="Ajouter fichier" multiple="multiple" / name="images[]">
                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $image }}</strong>
                                    </span>
                                @enderror
                         </div>
                        
                         @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      <div class="form-group">
                        <label for="inputName2">Instructions</label>
                        <div class="col-sm-10">
                        <select required="" class="form-control select2 @error('inst') is-invalid @enderror" name="inst" value="{{ old('inst') }}" autocomplete="inst" autofocus>
                         @foreach($insts as $inst)
                         @if(!empty($inst ->aqui))
                            <option value="{{$inst->inst}}" >
                              {{$inst->inst}}</option>
                          @endif
                         @endforeach
                        </select>
                      </div> 
                        
                         @error('inst')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      <div class="form-group">
                            <nav class="btn btn-primary" id="bnewinst"><i class="fa fa-plus"></i>Ajouter une nouvelle instruction</nav>
                            <div class="col-sm-10" style="display: none;" id="dnewinst">
                            <label for="newinst">Ajouter une nouvelle instruction</label>
                            <input class="form-control" name="newinst" id="newinst">
                          </div>
                          </div> 

                      <div class="form-group" hidden="hidden">
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('etat_id') is-invalid @enderror" name="etat_id"  autocomplete="etat_id" autofocus>
                          <option value="{{ old('etat_id') }}">Selectionner</option>
                         @foreach($etats as $etat)

                          @if($role::role(Auth::user()->role_id) != "srt" && $etat->libelle == "Affecté")
                                  <option  selected="selected" value="{{ $etat->id }}" >{{ $etat->libelle }}</option>
                          @endif

                          @if($role::role(Auth::user()->role_id) == "srt" && $etat->libelle == "Transmit")
                                  <option  selected="selected" value="{{ $etat->id }}" >{{ $etat->libelle }}</option>
                          @endif


                        @endforeach 

                        </select>
                      </div>
                         @error('destinataire_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>


                      </div>
                        <div class="col-md-6">      
                      <div class="form-group ">
                        <label for="inputName2" >Destinataires(s)</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('aqui') is-invalid @enderror" name="aqui[]" value="{{ old('aqui') }}" required autocomplete="aqui" autofocus multiple="multiple" data-placeholder="Selectionner">

                         @foreach($users as $user)
                         @if($role::role(Auth::user()->role_id) == "srt" && $role::role($user->role_id)== "sadmin" )
                         
                            <option value="{{ $user->id }}" >
                              {{ $user->nom }} {{ $user->prenom }}</option>
                         
                         @endif
                         @if($role::role(Auth::user()->role_id) != "srt" && $role::role($user->role_id)!= "sadmin" && $user->statut != 1 )
                            <option value="{{ $user->id }}" >
                              {{ $user->nom }} {{ $user->prenom }}</option>
                              
                         @endif
                         @endforeach
                        </select>
                      </div>
                        
                         @error('aqui')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      @if($role::role(Auth::user()->role_id) != "srt")

                      <div class="form-group ">
                        <label for="inputName" >Délai de traitement</label>                                          
                        <div class="input-group col-sm-10">
                        <div class="row">
                          <div class="col-md-4">
                           Jours <input type="number" class="form-control float-right  @error('Jours') is-invalid @enderror" value="{{ old('Jours') }}" autocomplete="Jours" autofocus name="Jours" id="Jours">
                          
                             @error('Jours')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                             @enderror
                            </div>
                            <div class="col-md-4">
                           Heures <input type="number" class="form-control float-right  @error('Heures') is-invalid @enderror" value="{{ old('Heures') }}" autocomplete="Heures" autofocus  id="Heures" name="Heures">
                          
                             @error('Heures')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                             @enderror
                            </div>
                            <div class="col-md-4">
                           Minuites <input type="number" class="form-control float-right  @error('Minuites') is-invalid @enderror" value="{{ old('Minuites') }}" required="" autocomplete="Minuites" autofocus  id="Minuites" name="Minuites">
                          
                             @error('Minuites')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                             @enderror
                            </div>
                          </div>  
                          </div>
                        </div>   
                           
                      @endif
                      <div class="form-group ">
                        <label for="inputName" >Note</label>
                        <div class="col-sm-10">
                          <textarea class="form-control @error('instructions') is-invalid @enderror" id="inputName" name="instructions" value="{{ old('intructions') }}"  autocomplete="instructions" autofocus></textarea>
                        </div>
                         @error('instructions')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                      </div>  
                      </div>
                      </div>     
                      <div class="form-group row">
                       <div class="container">
                        @if($role::role(Auth::user()->role_id) == "srt")
                          <button type="submit" id="bouton_valider" name="Transmettre" value="Transmettre" class="btn btn-primary">Transmettre</button>
                        @endif
                        @if($role::role(Auth::user()->role_id) != "srt")
                          <button type="submit" id="bouton_valider" name="Affecter" value="Affecter" class="btn btn-primary">Affecter</button>
                        @endif
                      </div>
                      </div>
                      </div>                    
                      
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection


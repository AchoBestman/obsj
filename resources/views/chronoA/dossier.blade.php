@extends('layouts.app1')
@section('main')
@inject('typearchi', 'App\Helpers\Helpers')
@inject('courid', 'App\Helpers\Helpers')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Contenu du dossier</a></li>
                  
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">
               <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td>Actions</td>
                      <th>Numero du courrier</th>
                      <th>Type d'archivage</th>
                     
                      <th>Date d'expiration</th>
                      <th>Note</th>

                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <td>Action</td>
                      <th>Numero</th>
                      <th>Type d'archivage</th>
                     
                      <th>Date d'expiration</th>
                      <th>Note</th>                 
                    </tr>
                  </tfoot>
                  <tbody>
                     
                    @foreach($courriers as $courrier)
                    <tr>
                     <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                             <a class="dropdown-item" href="{{ route('Courrier.show', $courid::courid($courrier->numero)) }}">Consulter</a> 
                           </form>                       
                           </div></div></td>
                      <td> {{ $courrier->numero }}</td>
                      <td>{{$typearchi::archi($courrier->typearchi)}}
                      </td>
                      
                        <td> {{ $courrier->datefinarchi }}</td>
                      <td> {{ $courrier->instructions }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
                  </div>
                  <!-- /.tab-pane -->


                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection
@include('home.draculum')
@inject('gestion', 'App\Helpers\Helpers')
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                @if($gestion::role(Auth::user()->role_id) != "sadmin")

                <h3>
                  @if($gestion::affectation_total_me(Auth::user()->id) == 0 )
                  {{$gestion::reaffectation_total_me(Auth::user()->id)}}</h3>
                  @endif
                   @if($gestion::affectation_total_me(Auth::user()->id) != 0 )
                  {{$gestion::affectation_total_me(Auth::user()->id)}}</h3>
                  @endif


                @else
                <h3>{{$gestion::affectation_total_admin(Auth::user()->id)}}</h3>
                @endif
                <p>Nombre d'affectation total</p>
              </div> 
              <div class="icon">
                <i class="fa fa-envelope text-white"></i>
              </div>
              <a href="{{route('Affectation.index')}}" class="small-box-footer">Consulter<i class="fas fa-envelop"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                 @if($gestion::role(Auth::user()->role_id) != "sadmin")

                <h3>{{$gestion::affectation_total_me_traite (Auth::user()->id)}}</h3>
                @else
                <h3>{{$gestion::affectation_total_admin_traite(Auth::user()->id)}}</h3>
                @endif

                <p>Nombre d'affectation traitées</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope text-white"></i>
              </div>
              <a href="{{route('Affectation.index')}}" class="small-box-footer">Consulter<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                  @if($gestion::role(Auth::user()->role_id) != "sadmin")

                <h3>{{$gestion::affectation_total_me_entraite (Auth::user()->id)}}</h3>
                @else
                <h3>{{$gestion::affectation_total_admin_entraite(Auth::user()->id)}}</h3>
                @endif

                <p>Nombre d'affectation en cours</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope text-white"></i>
              </div>
              <a href="{{route('Affectation.index')}}" class="small-box-footer">Consulter<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                 @if($gestion::role(Auth::user()->role_id) != "sadmin")

                <h3>{{$gestion::affectation_total_me_close (Auth::user()->id)}}</h3>
                @else
                <h3>{{$gestion::affectation_total_admin_close(Auth::user()->id)}}</h3>
                @endif

                <p>Nombre d'affectation closes</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope text-white"></i>
              </div>
              <a href="{{route('Affectation.index')}}" class="small-box-footer">Consulter<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <br>
      <p style="text-align: center;">
         <img src="/templates/dist/img/logo akpakpa.jpg" alt="AdminLTE Logo">
       </p>

      </div>
      <!-- /.container-fluid -->
    </div>

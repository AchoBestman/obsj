@component('mail::message')

<h3>Notification de courriers clos</h3>

Bonjour Mr/Mme,

Vous recevez cet email car un courrier  à été clos. <br> Merci de vous connecter afin de prendre connaissance de ce dernier et de l'archiver. <br>
Merci.






Ce email est automatique. merci de ne pas répondre.<br>

{{ 'Plateforme de Gestion des Courriers - OBSVJ' }}
@endcomponent

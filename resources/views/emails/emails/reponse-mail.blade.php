@component('mail::message')

<h3>Notification de traitement de courrier</h3>
<p>Une modification a été apportée au courrier. Veuillez vérifier le contenu.</p>

{{$reponse}}

 
Ce email est automatique. merci de ne pas répondre.<br>
{{ 'Gestion des Courriers - OBSVJ' }}
@endcomponent

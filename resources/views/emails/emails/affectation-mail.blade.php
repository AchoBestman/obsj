@component('mail::message')

<h3>Notification d'affectation de Courrier</h3>

Bonjour Mr/Mme,

Vous recevez cet email car un courrier vous à été affecter pour traitement. <br> Merci de vous connecter afin de prendre connaissance de ce dernier et de le traiter avant le <em style="color: red;">{{$items}}</em><br>
Une fois cette date passée, vous ne pouvez plus traiter ce courrier. <br>
Merci.






Ce email est automatique. merci de ne pas répondre.<br>

{{ 'Plateforme de Gestion des Courriers - OBSVJ' }}
@endcomponent

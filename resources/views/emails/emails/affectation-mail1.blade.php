@component('mail::message')

<h3>Notification de réaffectation de Courrier</h3>

Bonjour Mr/Mme,

Vous recevez cet email car un courrier vous à été réaffecté pour traitement. <br> Merci de vous connecter afin de prendre connaissance de ce dernier. <br>
Merci.






Ce email est automatique. merci de ne pas répondre.<br>

{{ 'Plateforme de Gestion des Courriers - OBSVJ' }}
@endcomponent

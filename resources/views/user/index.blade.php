@extends('layouts.app1')
@section('main')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Liste du Personel</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Ajouter</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <!-- /.tab-pane -->
                  <div class="active tab-pane" id="timeline">
             <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Nom</th>
                      <th>Prénoms</th>
                      <th>Email</th>                     
                     
                      <th>Date de Naissances</th>
                      <th>Lieu de Naissance</th>
                      <th>Contacts</th>
                      <th>Adresses</th>
                      <th>Fonction</th>
                      <th>Bureau</th>
                      <th>Rôle</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Noms</th>
                      <th>Prénoms</th>
                      <th>Emails</th>                     
                      
                      <th>Date de Naissances</th>
                      <th>Lieu de Naissance</th>
                      <th>Contacts</th>
                      <th>Adresses</th>
                      <th>Fonctions</th>
                      <th>Bureaux</th>
                      <th>Rôles</th>
                      <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($users as $user)
                    <tr>
                      <td> {{ $user->nom }}</td>
                      <td> {{ $user->prenom }}</td>
                      <td> {{ $user->email }}</td> 
                     
                      <td> {{ $user->dateNaissance }}</td>
                      <td> {{ $user->lieuNaissance }}</td>
                      <td> {{ $user->telephone }}</td>
                      <td> {{ $user->adresse }}</td>
                      <td> {{$user->fonction? $user->fonction->libelle:''}}</td>
                      <td> {{$user->office? $user->office->libelle:'' }}</td>
                      <td> {{ $user->role? $user->role->libelle:'' }}</td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('User.edit', $user->id) }}">Modifier</a> 
                            
                             @if($user->statut != 1)                
                            <form method="GET" action="{{ route('statut', $user->id) }}">
                             @csrf
                             <input type="hidden" name="statut" value="{{1}}">
                             
                            <button  class="dropdown-item">Agent Inactif</button> 

                           </form> 
                           @else
                           <form method="GET" action="{{ route('statut', $user->id) }}">
                             @csrf
                             <input type="hidden" name="statut" value="{{0}}">
                             
                            <button  class="dropdown-item">Agent actif</button> 

                           </form> 
                           @endif
                           
                          </div>
                        </div>
                      </td>

                    </tr>
                  
                    @endforeach 
                  </tbody>
                </table>
              </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <form method="POST" action="{{ route('User.store') }}">
                       @csrf
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="inputName">Nom</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control  @error('nom') is-invalid @enderror" id="inputName" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus>

                                @error('nom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>

                          <div class="form-group">
                            <label for="inputName">Sexe</label>
                            <div class="col-sm-10">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input  @error('masculin') is-invalid @enderror" type="checkbox" id="inlineCheckbox1" value="{{ old('masculin') }}" autocomplete="name" autofocus name="sexe">
                                <label class="form-check-label" for="inlineCheckbox1">Masculin</label>

                                 @error('masculin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input @error('feminin') is-invalid @enderror" type="checkbox" id="inlineCheckbox2" name="sexe" value="{{ old('feminin') }}" autocomplete="feminin" autofocus>
                                <label class="form-check-label" for="inlineCheckbox2">Féminin</label>

                                 @error('feminin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="lieu">Lieu de Naissance</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('lieu') is-invalid @enderror" id="inputName"  name="lieuNaissance"  value="{{ old('lieu') }}"  autocomplete="lieu" autofocus>
                              </div>
                              @error('lieu')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="inputName">Email</label>
                              <div class="col-sm-10">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="inputName"  name="email"  value="{{ old('email') }}" required autocomplete="email" autofocus>
                              </div>                         
                           @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                         <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Fonction</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('fonction_id') is-invalid @enderror" name="fonction_id" required autocomplete="fonction_id" autofocus >
                          <option value="{{ old('fonction_id')?? 0 }}">Selectionner</option>
                          @foreach($fonctions as $fonction)
                            <option value="{{ $fonction->id }}" >
                              {{ $fonction->libelle }}</option>
                         @endforeach
                        </select>
                      </div>
                        
                         @error('fonction_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Adresse</label>
                        <div class="col-sm-10">
                        <input class="form-control @error('adresse') is-invalid @enderror" type="text" name="adresse" value="{{ old('adresse') }}"  autocomplete="adresse" autofocus placeholder="adresse du Personel">
                         
                      </div>
                        
                         @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                             
                      </div>
                     
                      
                       <div class="col-md-6">
                          <div class="form-group">
                            <label for="inputName">Prénom</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('prenom') is-invalid @enderror" id="inputName" placeholder="Prénom" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" autofocus>
                              </div>
                              @error('prenom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>                         

                          <div class="form-group">
                            <label for="inputName">Date de Naissance</label>
                              <div class="col-sm-10">
                                <input type="date" class="form-control @error('dateNaissance') is-invalid @enderror" id="inputName"  name="dateNaissance" value="{{ old('dateNaissance') }}"  autocomplete="dateNaissance" autofocus>
                              </div>
                              @error('dateNaissance')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="inputName">Contacts</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="inputName" placeholder="Contacts" name="telephone" value="{{ old('telephone') }}" required autocomplete="telephone" autofocus>
                              </div>
                              @error('telephone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="inputName">Mot de Passe</label>
                              <div class="col-sm-10">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="inputName" placeholder="mot de passe" name="password" value="{{ old('password') }}"  autocomplete="password" autofocus>
                              </div>
                              @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="password" class="">{{ __('Confirmation') }}</label>

                            <div class="col-sm-10">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                       <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Rôle</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('role_id') is-invalid @enderror" name="role_id" required autocomplete="role_id" autofocus>
                          <option value="{{ old('role_id')?? 0 }}">Selectionner</option>
                           @foreach($roles as $role)
                            <option value="{{ $role->id }}" >
                              {{ $role->libelle }}</option>
                         @endforeach
                        </select>
                      </div>
                        
                         @error('role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>
                                               <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Bureau</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('office_id') is-invalid @enderror" name="office_id" autocomplete="office_id" autofocus>
                          <option value="{{ old('office_id')?? 0 }}">Selectionner</option>
                          @foreach($offices as $office)
                            <option value="{{ $office->id }}" >
                              {{ $office->libelle }}</option>
                         @endforeach
                        </select>
                      </div>
                        
                         @error('office_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                              
                        </div>
         
                      <div class="container" style="max-width: 200px;">
                          <button type="submit" class="btn btn-primary form-control">Ajouter</button>
                      </div>
                    </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection
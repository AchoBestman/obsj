@extends('layouts.app1')
@inject('gestion', 'App\Helpers\Helpers')
@inject('profile', 'App\Helpers\Helpers')
@section('main')
<br>

  <div class="container">
     @foreach($users as $user)
  <h3 style="text-align: center;">Mon Compte - Compte {{ $user->role?$user->role->libelle:'' }} </h3>

    <div class="col-md-12 col-sm-12 col-lg-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img src="/templates/dist/img/avatar5.png" class="profile-user-img img-fluid img-circle ">                
                </div>
                
                
                <h3 class="profile-username text-center">{{$user->nom }} {{$user->prenom }}</h3>

                <p class="text-muted text-center">{{ $user->fonction?$user->fonction->libelle:'' }}</p>
                <p class="text-muted text-center">{{ $user->office?$user->office->libelle:'' }}</p>
                 <p class="text-muted text-center">{{$user->office?$profile::departement($user->office->departement_id):''}}</p>
                  <p class="text-muted text-center">{{$user->office?$profile::directiont($profile::departement_id($user->office->departement_id)):''}}</p>
                   <p class="text-muted text-center">{{$user->office?$profile::direction($profile::directiont_id($user->office->departement_id)):''}}</p>
                      

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Nombre d'affectation total</b> <a class="float-right"> 
                 @if($gestion::role(Auth::user()->role_id) != "clt")

                {{$gestion::affectation_total_admin(Auth::user()->id)}}
                @else
                {{$gestion::affectation_total_me(Auth::user()->id)}}
                @endif</a>
                  </li>
                  <li class="list-group-item">
                    <b>Nombre d'affectation en cours</b> <a class="float-right">
              @if($gestion::role(Auth::user()->role_id) != "clt")

               {{$gestion::affectation_total_admin_entraite(Auth::user()->id)}}
                @else
               {{$gestion::affectation_total_me_entraite(Auth::user()->id)}}
                @endif
                    </a>
                  </li>
                  <li class="list-group-item">
                    <b>Nombre d'affectation traitée</b> <a class="float-right">
                      @if($gestion::role(Auth::user()->role_id) != "clt")

                {{$gestion::affectation_total_admin_traite(Auth::user()->id)}}
                @else
                {{$gestion::affectation_total_me_traite(Auth::user()->id)}}
                @endif
              </a>
                  </li>
                  <li class="list-group-item">
                    <b>Nombre d'affectation closes</b> <a class="float-right">
                      @if($gestion::role(Auth::user()->role_id) != "clt")

                {{$gestion::affectation_total_admin_close(Auth::user()->id)}}
                @else
                {{$gestion::affectation_total_me_close(Auth::user()->id)}}
                @endif

                    </a>
                  </li>
                </ul>

                <a href="/" class="btn btn-primary btn-block"><b>Tableau de bord</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">A propos de Moi</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Date & Lieu de naissance</strong>

                <p class="text-muted">
                  {{$user->dateNaissance }} à {{$user->lieuNaissance }}
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Adresse</strong>

                <p class="text-muted">{{$user->adresse }}</p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Bureau</strong>
                <p class="text-muted">{{ $user->office?$user->office->libelle:'' }}</p>

                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Email & Contact</strong>

                <p class="text-muted">{{$user->email }}, {{$user->telephone }}</p>
                <hr>
                <a class="btn btn-block btn-primary" href="{{ route('User.edit', $user->id) }}">Modifier</a> 
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

</div>
@endforeach
<br>

@endsection
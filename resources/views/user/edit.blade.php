@extends('layouts.app1')
@section('main')
@inject('role', 'App\Helpers\Helpers')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Modifier agent</a></li>                 
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
 
                  <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">
                @foreach($users as $user)
                <form method="POST" action="{{ route('User.update', $user->id) }}">
                       @csrf
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="inputName">Nom</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control  @error('nom') is-invalid @enderror" id="inputName" name="nom" value="{{$user->nom }}" required autocomplete="nom" autofocus>

                                @error('nom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>

                          <div class="form-group">
                            <label for="inputName">Sexe</label>
                            <div class="col-sm-10">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input  @error('masculin') is-invalid @enderror" type="checkbox" id="inlineCheckbox1" value="{{ old('masculin') }}" autocomplete="sexe" autofocus name="sexe">
                                <label class="form-check-label" for="inlineCheckbox1">Masculin</label>

                                 @error('masculin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input @error('feminin') is-invalid @enderror" type="checkbox" id="inlineCheckbox2" name="sexe" value="{{ old('feminin') }}" autocomplete="feminin" autofocus>
                                <label class="form-check-label" for="inlineCheckbox2">Féminin</label>

                                 @error('feminin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="lieu">Lieu de Naissance</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('lieu') is-invalid @enderror" id="inputName"  name="lieuNaissance"  value="{{$user->lieuNaissance }}" required autocomplete="lieu" autofocus>
                              </div>
                              @error('lieu')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="inputName">Email</label>
                              <div class="col-sm-10">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="inputName"  name="email"  value="{{$user->email }}" required autocomplete="email" autofocus>
                              </div>                         
                           @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        @if($role::role(Auth::user()->role_id) == "sadmin")
                         <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Fonction</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('fonction_id') is-invalid @enderror" name="fonction_id"  required autocomplete="fonction_id" autofocus>
                          <option value="{{$user->fonction_id }}" selected="">Selectionner</option>
                          @foreach($fonctions as $fonction)
                            <option value="{{ $fonction->id }}" >
                              {{ $fonction->libelle }}</option>
                         @endforeach
                        </select>
                      </div>
                        
                         @error('fonction_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      @endif
                      <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Adresse</label>
                        <div class="col-sm-10">
                        <input class="form-control @error('adresse') is-invalid @enderror" type="text" name="adresse" value="{{$user->adresse }}" required autocomplete="adresse" autofocus>
                         
                      </div>
                        
                         @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                             
                      </div>
                     
                      
                       <div class="col-md-6">
                          <div class="form-group">
                            <label for="inputName">Prénom</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('prenom') is-invalid @enderror" id="inputName" placeholder="Prénom" name="prenom" value="{{$user->prenom }}" required autocomplete="prenom" autofocus>
                              </div>
                              @error('prenom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>                         

                          <div class="form-group">
                            <label for="inputName">Date de Naissance</label>
                              <div class="col-sm-10">
                                <input type="date" class="form-control @error('dateNaissance') is-invalid @enderror" id="inputName"  name="dateNaissance" value="{{$user->dateNaissance }}" required autocomplete="dateNaissance" autofocus>
                              </div>
                              @error('dateNaissance')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="inputName">Contacts</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="inputName" placeholder="Contacts" name="telephone" value="{{$user->telephone }}"  autocomplete="telephone" autofocus>
                              </div>
                              @error('telephone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="inputName">Mot de Passe</label>
                              <div class="col-sm-10">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="inputName" placeholder="mot de passe" name="password" value="{{$user->passwordnc }}"  autocomplete="password" autofocus>
                              </div>
                              @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>

                          <div class="form-group">
                            <label for="password" class="">{{ __('Confirmation') }}</label>

                            <div class="col-sm-10">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        @if($role::role(Auth::user()->role_id) == "sadmin")
                       <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Rôle</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('role_id') is-invalid @enderror" name="role_id"  required autocomplete="role_id" autofocus>
                          <option value="{{$user->role_id }}" selected="">Selectionner</option>
                           @foreach($roles as $role)
                            <option value="{{ $role->id }}" >
                              {{ $role->libelle }}</option>
                         @endforeach
                        </select>
                      </div>
                        
                         @error('role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>
                      
                        <div class="form-group">
                        <label for="inputName2" class="col-sm-2 col-form-label">Bureau</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('office_id') is-invalid @enderror" name="office_id"  required autocomplete="office_id" autofocus>
                          <option value="{{$user->office_id }}" selected="">Selectionner</option>
                          @foreach($offices as $office)
                            <option value="{{ $office->id }}">
                              {{ $office->libelle }}</option>
                         @endforeach
                        </select>
                      </div>
                        
                         @error('office_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                        @endif  
                        </div>
         
                      <div class="container" style="max-width: 200px;">
                          <button type="submit" class="btn btn-primary form-control">Modifier</button>
                      </div>
                    </div>
                    {{method_field('PUT')}}
                    </form>
                    @endforeach
                  </div>
               
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection
@extends('layouts.app1')
@section('main')
@inject('role', 'App\Helpers\Helpers')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Modifier</a></li>                 
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
 
                  <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">
                @foreach($courriers as $courrier)
                <form class="form-horizontal" method="POST" action="{{ route('Courrier.update', $courrier->id) }}">
                       @csrf
                      <div class="row">
                        <div class="col-md-6">
                          @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                          <div class="form-group">
                            <label for="nompr">Numero primaire</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control  @error('nompr') is-invalid @enderror" id="inputName" name="nompr" value="{{ $courrier->numeroPrimaire}}." required autocomplete="nompr" autofocus>

                                @error('nompr')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>
                          @endif
                          <div class="form-group">
                          <div class="form-group">
                            <label for="nomeneg">Numero D'enregistrement</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control  @error('nomeneg') is-invalid @enderror" id="inputName" name="nomeneg" value="{{$courrier->numeroEnregistrement}}" required autocomplete="nomeneg" autofocus>

                                @error('nomeneg')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>
                          </div>

                       <div class="form-group">
                        <label for="chrono_id">Chrono</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('chrono_id') is-invalid @enderror" name="chrono_id"  required autocomplete="chrono_id" autofocus>
                          <option value="{{$courrier->chrono_id }}" selected="">{{$courrier->chrono->libelle }}</option>
                           @foreach($chronos as $chrono)
                            <option value="{{ $chrono->id }}" >
                              {{ $chrono->libelle }}</option>

                         @endforeach                        
                        </select>
                      </div>
                        
                         @error('chrono_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 

                          <div class="form-group">
                            <label for="objet">Objet</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('objet') is-invalid @enderror" id="inputName"  name="objet"  value="{{ $courrier->objet }}" required autocomplete="objet" autofocus>
                              </div>                         
                           @error('objet')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                         <div class="form-group">
                        <label for="confidentiel">Confidentiel</label>
                        <div class="col-sm-10">
                        <select class="form-control @error('confidentiel') is-invalid @enderror" name="confidentiel"  required autocomplete="confidentiel" autofocus>
                          <option value="{{ $courrier->confidentiel }}" selected="">{{ $courrier->confidentiel}}</option>
                          <option>Oui</option>
                          <option>Non</option>                         
                        </select>
                      </div>
                        
                         @error('confidentiel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      <div class="form-group">
                        <label for="anotation">Anotation</label>
                        <div class="col-sm-10">
                        <input class="form-control @error('anotation') is-invalid @enderror" type="text" name="anotation" value="{{ $courrier->annotations }}." required autocomplete="anotation" autofocus>
                         
                      </div>
                        
                         @error('anotation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                       <div class="form-group">
                        <label for="reference">Référence</label>
                        <div class="col-sm-10">
                        <input class="form-control @error('reference') is-invalid @enderror" type="text" name="reference" value="{{ $courrier->reference }}" required autocomplete="reference" autofocus>
                         
                      </div>
                        
                         @error('reference')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      </div>
                     
                      
                       <div class="col-md-6">
                         <div class="form-group">
                            <label for="type_id">Type de document</label>
                            <div class="col-sm-10">
                            <select class="form-control select2 @error('type_id') is-invalid @enderror" name="type_id"  required autocomplete="type_id" autofocus>
                              <option value="{{ $courrier->type_id }}" selected="">{{ $courrier->type->libelle}}</option>
                               @foreach($types as $type)
                            <option value="{{ $type->id }}" >
                              {{ $type->libelle }}</option>

                         @endforeach                         
                            </select>
                          </div>
                            
                             @error('type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div> 
                          <div class="form-group">
                            <label for="nature_id">Nature du document</label>
                            <div class="col-sm-10">
                            <select class="form-control @error('nature_id') is-invalid @enderror" name="nature_id"  required autocomplete="nature_id" autofocus>
                              <option value="{{ $courrier->nature_id }}" selected="">{{ $courrier->nature->libelle }}</option>
                               @foreach($natures as $nature)
                            <option value="{{ $nature->id }}" >
                              {{ $nature->libelle }}</option>

                         @endforeach                       
                            </select>
                          </div>
                            
                             @error('nature_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div> 
                          @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                           <div class="form-group">
                            <label for="priorite_id">Priorité</label>
                            <div class="col-sm-10">
                            <select class="form-control @error('priorite_id') is-invalid @enderror" name="priorite_id"  required autocomplete="priorite_id" autofocus>
                              <option value="{{ $courrier->priorite_id }}" selected="">{{ $courrier->priorite->libelle }}</option>
                               @foreach($priorites as $priorite)
                            <option value="{{ $priorite->id }}" >
                              {{ $priorite->libelle }}</option>

                         @endforeach                         
                            </select>
                          </div>
                            
                             @error('priorite_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div> 

                          <div class="form-group">
                            <label for="expediteur_id">Expéditeur</label>
                            <div class="col-sm-10">
                            <select class="form-control select2 @error('expediteur_id') is-invalid @enderror" name="expediteur_id" required autocomplete="expediteur_id" autofocus>
                              <option value="{{ $courrier->expediteur_id }}" selected="">{{ $courrier->expediteur->nom.' '. $courrier->expediteur->prenom}}</option>
                              @foreach($users as $user)
                                  <option value="{{ $user->id }}" >
                                    {{ $user->nom }} {{ $user->prenom }}</option>
                               @endforeach                         
                            </select>
                          </div>
                             @error('expediteur_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div>  
                       <div class="form-group">
                        <label for="destinataire_id">Destinataire</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('destinataire_id') is-invalid @enderror" name="destinataire_id" required autocomplete="destinataire_id" autofocus>
                          <option value="{{$courrier->destinataire_id }}" selected="">{{$courrier->destinataire->nom.' '.$courrier->destinataire->prenom }}</option>
                         @foreach($users as $user)
                                  <option value="{{ $user->id }}" >
                                    {{ $user->nom }} {{ $user->prenom }}</option>
                               @endforeach  
                        </select>
                      </div>
                        
                         @error('destinataire_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      @endif
                     <div class="form-group" hidden="">
                        <label for="cournumerise">Document Numérisé</label>
                        <div class="col-sm-10">
                        <input type="file" class="form-control @error('cournumerise') is-invalid @enderror" name="cournumerise" value="{{ $courrier->courriersNumerise }}"  autocomplete="cournumerise" autofocus>                                                
                      </div>
                        
                         @error('cournumerise')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                              
                        </div>
         
                      <div class="container" style="max-width: 200px;">
                          <button type="submit" class="btn btn-primary form-control">Modifier</button>
                      </div>
                    </div>
                     {{ method_field('PUT') }}
                    </form>
                    @endforeach
                  </div>
               
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection
@extends('layouts.app1')
@inject('role', 'App\Helpers\Helpers')
@inject('users', 'App\Helpers\Helpers')
@inject('role', 'App\Helpers\Helpers')
@section('main')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">

                  <li class="nav-item"><a class="nav-link active" href="#ajouter" data-toggle="tab">Ajouter</a></li>

                  @if($role::role(Auth::user()->role_id) == "mp" ||  $role::role(Auth::user()->role_id) == "comp")
                  <li class="nav-item"><a class="nav-link" href="#liste_doc" data-toggle="tab">Liste</a></li>
                  @endif

                  @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                  <li class="nav-item"><a class="nav-link " href="#arrive" data-toggle="tab">Liste des Courriers Arrivés
                  
                  </a></li>                     
                 
                  <li class="nav-item"><a class="nav-link" href="#depart" data-toggle="tab">Liste des Courriers Départs</a></li>
                  <li class="nav-item"><a class="nav-link" href="#nontraité" data-toggle="tab">Liste des Courriers non traités</a></li>
                  
                   <li class="nav-item"><a class="nav-link" href="#archive" data-toggle="tab">Archives</a></li>
                    @if($role::role(Auth::user()->role_id) == "sadmin")
                   <li class="nav-item"><a class="nav-link" href="#supprime" data-toggle="tab">Corbeille</a></li>
                    @endif

                   @endif
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="arrive">
             <div class="table-responsive">
               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Numero</th>
                      <th>Référence</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                    
                      <th>Actions</th>                  
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Numero</th>
                      <th>Référence</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                    
                       <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($courriers as $courrier)
                     @if($courrier->type && $courrier->type->libelle == "Arrivé" && $courrier->archive != 1 && $courrier->corbeille != 1)
                    <tr>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                      <td> {{ $courrier->priorite?$courrier->priorite->libelle:'' }}</td>
                      <td> {{ $courrier->chrono?$courrier->chrono->libelle:'' }}</td>
                       <td> {{ $courrier->confidentiel }}</td>
                      <td> {{  $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->expediteur->prenom:'' }}</td>
                      <td> {{ $courrier->expediteur?$courrier->destinataire->nom:'' }} {{ $courrier->expediteur?$courrier->destinataire->prenom:'' }}</td>
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('Courrier.show', $courrier->id) }}">Consulter</a>  
                            <a class="dropdown-item" href="{{ route('Courrier.edit', $courrier->id) }}">Modifier</a>  
                            <form method="GET" action="{{ route('archive', $courrier->id) }}">
                             @csrf
                             <input type="hidden" name="archive" value="{{1}}">
                            <button  class="dropdown-item"> Archiver</button> 
                           </form>                      
                              @if($role::role(Auth::user()->role_id) == "srt")
                             <a class="dropdown-item" href="{{ route('Affectation.show', $courrier->id) }}">Transmettre</a> 
                             @else
                             <a class="dropdown-item" href="{{ route('Affectation.show', $courrier->id) }}">Affecter</a> 
                             @endif
                          </div></div></td>
                        </tr>
                        @endif
                        @endforeach   
                  </tbody>
                </table>
              </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="depart">
             <div class="table-responsive">
               <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                      <th>Actions</th>                  
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                       <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($courriers as $courrier)
                     @if($courrier->type && $courrier->type->libelle == "Depart" && $courrier->archive != 1 && $courrier->corbeille != 1)
                    <tr>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                      <td> {{ $courrier->priorite?$courrier->priorite->libelle:'' }}</td>
                      <td> {{ $courrier->chrono?$courrier->chrono->libelle:'' }}</td>
                       <td> {{ $courrier->confidentiel }}</td>
                      <td> {{  $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->expediteur->prenom:'' }}</td>
                      <td> {{ $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->destinataire->prenom:'' }}</td>
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('Courrier.show', $courrier->id) }}">Consulter</a>  
                            <a class="dropdown-item" href="{{ route('Courrier.edit', $courrier->id) }}">Modifier</a>  
                            <form method="GET" action="{{ route('archive', $courrier->id) }}">
                             @csrf
                             <input type="hidden" name="archive" value="{{1}}">
                            <button  class="dropdown-item"> Archiver</button> 
                           </form>
                              @if($role::role(Auth::user()->role_id) == "srt")
                             <a class="dropdown-item" href="{{ route('Affectation.show', $courrier->id) }}">Transmettre</a> 
                             @else
                             <a class="dropdown-item" href="{{ route('Affectation.show', $courrier->id) }}">Affecter</a> 
                             @endif
                           </div></div></td>
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>

                                    <!-- /.tab-pane -->

                  <div class="tab-pane" id="nontraité">
             <div class="table-responsive">
               <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                      <th>Actions</th>                  
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                       <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($courriers as $courrier)
                     @if($courrier->etat_id != 1)
                    <tr>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                      <td> {{ $courrier->priorite?$courrier->priorite->libelle:'' }}</td>
                      <td> {{ $courrier->chrono?$courrier->chrono->libelle:'' }}</td>
                       <td> {{ $courrier->confidentiel }}</td>
                      <td> {{  $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->expediteur->prenom:'' }}</td>
                      <td> {{ $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->destinataire->prenom:'' }}</td>
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('Courrier.show', $courrier->id) }}">Consulter</a>  
                            <a class="dropdown-item" href="{{ route('Courrier.edit', $courrier->id) }}">Modifier</a>  
                            <form method="GET" action="{{ route('archive', $courrier->id) }}">
                             @csrf
                             <input type="hidden" name="archive" value="{{1}}">
                            <button  class="dropdown-item"> Archiver</button> 
                           </form>
                              @if($role::role(Auth::user()->role_id) == "srt")
                             <a class="dropdown-item" href="{{ route('Affectation.show', $courrier->id) }}">Transmettre</a> 
                             @else
                             <a class="dropdown-item" href="{{ route('Affectation.show', $courrier->id) }}">Affecter</a> 
                             @endif
                           </div></div></td>
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>

                                                  <!-- /.tab-pane -->
          <div class="tab-pane" id="archive">
             <div class="table-responsive">
               <table id="example4" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                      <th>États de Traitement</th> 
                       <th>Date d'archivage</th>
                      <th>Actions</th>                  
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                      <th>États de Traitement</th>
                      <th>Date d'archivage</th>
                       <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($courriers as $courrier)
                     @if($courrier->archive == 1 && $courrier->corbeille != 1)
                    <tr>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                      <td> {{ $courrier->priorite?$courrier->priorite->libelle:'' }}</td>
                      <td> {{ $courrier->chrono?$courrier->chrono->libelle:'' }}</td>
                       <td> {{ $courrier->confidentiel }}</td>
                      <td> {{  $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->expediteur->prenom:'' }}</td>
                      <td> {{ $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->destinataire->prenom:'' }}</td>
                      
           
                      
                       <td> {{ $courrier->datearchive }}</td> 
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                             <a class="dropdown-item" href="{{ route('Courrier.show', $courrier->id) }}">Consulter</a> 
                              <form method="GET" action="{{ route('archive', $courrier->id) }}">
                             @csrf
                             <input type="hidden" name="archive" value="{{0}}">
                            <button  class="dropdown-item"> Désarchiver</button> 
                           </form>                       
                           </div></div></td>
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>



                  <div class="tab-pane" id="liste_doc">
             <div class="table-responsive">
               <table id="example4" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Actions</th>                  
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                       <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($document as $courrier)
                    <tr>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                      <td> {{ $courrier->chrono?$courrier->chrono->libelle:'' }}</td>
                       <td> {{ $courrier->confidentiel }}</td>
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                             <a class="dropdown-item" href="{{ route('Courrier.show', $courrier->id) }}">Consulter</a> 
                             <a class="dropdown-item" href="{{ route('Courrier.edit', $courrier->id) }}">Modifier</a>  
                          <form method="POST" action="{{ route('Courrier.destroy2', $courrier->id) }}">
                             @csrf
                            <button  class="dropdown-item"> Supprimer</button> 
                           </form>                       
                           </div></div></td>
                    </tr>
                    @endforeach            
                  </tbody> 
                </table>
              </div>
                  </div>
                                <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline1sss">
             <div class="table-responsive">
               <table id="example3" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                      <th>Actions</th>                  
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                       <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($courriers as $courrier)
                     @if($courrier->type && $courrier->type->libelle == "Départ")
                    <tr>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                      <td> {{ $courrier->priorite?$courrier->priorite->libelle:'' }}</td>
                      <td> {{ $courrier->chrono?$courrier->chrono->libelle:'' }}</td>
                       <td> {{ $courrier->confidentiel }}</td>
                      <td> {{  $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->expediteur->prenom:'' }}</td>
                      <td> {{ $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->destinataire->prenom:'' }}</td>
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('Courrier.edit', $courrier->id) }}">Modifier</a> 
                          </td>
                          </div>
                        </div></td>           
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>

                                                  <!-- /.tab-pane -->
          <div class="tab-pane" id="supprime">
             <div class="table-responsive">
               <table id="example5" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                      <th>États de Traitement</th> 
                       <th>Date d'archivage</th>
                      <th>Actions</th>                  
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      <th>Priorité</th>
                      <th>Chrono</th>
                      <th>Confidentiel</th>
                      <th>Expéditeurs</th>
                      <th>Destinataire</th>
                      <th>États de Traitement</th>
                      <th>Date d'archivage</th>
                       <th>Actions</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($courriers as $courrier)
                     @if($courrier->corbeille == 1)
                    <tr>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                      <td> {{ $courrier->priorite?$courrier->priorite->libelle:'' }}</td>
                      <td> {{ $courrier->chrono?$courrier->chrono->libelle:'' }}</td>
                       <td> {{ $courrier->confidentiel }}</td>
                      <td> {{  $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->expediteur->prenom:'' }}</td>
                      <td> {{ $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->destinataire->prenom:'' }}</td>
                      <td> {{ $courrier->etat?$courrier->etat->libelle:'' }}</td>
                       <td> {{ $courrier->datearchive }}</td> 
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                             <a class="dropdown-item" href="{{ route('Courrier.show', $courrier->id) }}">Consulter</a> 
                              <form method="GET" action="{{ route('archive', $courrier->id) }}">
                             @csrf
                             <input type="hidden" name="archive" value="{{0}}">
                             <input type="hidden" name="restaurer" value="{{0}}">
                            <button  class="dropdown-item"> Restaurer</button> 
                           </form>                       
                           </div></div></td>
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table> 
              </div>
                  </div>

                  <div class=" active tab-pane" id="ajouter">
                    <form method="POST" action="{{ route('Courrier.store') }}" enctype="multipart/form-data">
                       @csrf
                      <div class="row">
                        <div class="col-md-6">
                          @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                          <div class="form-group">
                            <label for="nompr">Numero primaire</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control  @error('nompr') is-invalid @enderror" id="inputName" name="nompr" value="{{ old('nompr') }}"  autocomplete="nompr" autofocus placeholder="Facultatif">

                                @error('nompr')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>
                            @endif
                          <div class="form-group">
                          <div class="form-group">
                            <label for="nomeneg">Numero D'enregistrement</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control  @error('nomeneg') is-invalid @enderror" id="inputName" name="nomeneg" value="{{ old('nomeneg') }}" required autocomplete="nomeneg" autofocus placeholder="numéro interne d'enregistrement du  document">

                                @error('nomeneg')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>
                          </div>
                            @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                       <div class="form-group">
                        <label for="enreponsea">En reponse à</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('enreponsea') is-invalid @enderror" name="enreponsea"  autocomplete="chrono_id" autofocus>
                          <option selected="" value="{{ old('enreponsea') }}.">Selectionner</option>
                           @foreach($courriers as $courrier_reponse)
                            <option value="{{ $courrier_reponse->id }}" >
                              {{ $courrier_reponse->reference }}</option>

                         @endforeach                        
                        </select>
                      </div>
                        
                         @error('enreponsea')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                        @endif
                      <div class="form-group">
                        <label for="chrono_id">Chrono</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('chrono_id') is-invalid @enderror" name="chrono_id" required autocomplete="chrono_id" autofocus>
                          <option value="{{ old('chrono_id') }}">Selectionner</option>
                           @foreach($chronos as $chrono)
                            <option value="{{ $chrono->id }}" >
                              {{ $chrono->libelle }}</option>

                         @endforeach                        
                        </select>
                      </div>
                        
                         @error('chrono_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 

                          <div class="form-group">
                            <label for="objet">Objet</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control @error('objet') is-invalid @enderror" id="inputName"  name="objet"  value="{{ old('objet') }}" required autocomplete="objet" autofocus placeholder="objet du courrier">
                              </div>                         
                           @error('objet')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                         <div class="form-group">
                        <label for="confidentiel">Confidentiel</label>
                        <div class="col-sm-10">
                        <select class="form-control @error('confidentiel') is-invalid @enderror" name="confidentiel"  required autocomplete="confidentiel" autofocus>
                          <option value="{{ old('confidentiel') }}">Selectionner</option>
                          <option>Oui</option>
                          <option selected="selected">Non</option>                         
                        </select>
                      </div>
                        
                         @error('confidentiel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      <div class="form-group">
                        <label for="anotation">Anotation</label>
                        <div class="col-sm-10">
                        <input class="form-control @error('anotation') is-invalid @enderror" type="text" name="anotation" value="{{ old('anotation') }}" placeholder="Annotation du DG (facltatif)" autocomplete="anotation" autofocus>
                         
                      </div>
                        
                         @error('anotation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                       <div class="form-group">
                        <label for="reference">Référence</label>
                        <div class="col-sm-10">
                        <input class="form-control @error('reference') is-invalid @enderror" type="text" name="reference" value="{{ old('reference') }}" required autocomplete="reference" autofocus placeholder="référence du courrier. Ex: MJSL/001/DG">
                         
                      </div>
                        
                         @error('reference')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                      </div>
                     
                      
                       <div class="col-md-6">
                         <div class="form-group">
                            <label for="type_id">Type de document</label>
                            <div class="col-sm-10">
                            <select class="form-control select2 @error('type_id') is-invalid @enderror" name="type_id" required autocomplete="type_id" autofocus>
                              <option value="{{ old('type_id') }}">Selectionner</option>
                               @foreach($types as $type)
                            <option value="{{ $type->id }}" >
                              {{ $type->libelle }}</option>

                         @endforeach                         
                            </select>
                          </div>
                            
                             @error('type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div> 
                          <div class="form-group">
                            <label for="nature_id">Nature du document</label>
                            <div class="col-sm-10">
                            <select class="form-control @error('nature_id') is-invalid @enderror" name="nature_id"  required autocomplete="nature_id" autofocus>
                              <option value="{{ old('nature') }}">Selectionner</option>
                               @foreach($natures as $nature)
                            <option value="{{ $nature->id }}" >
                              {{ $nature->libelle }}</option>

                         @endforeach                       
                            </select>
                          </div>
                            
                             @error('nature_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div> 
                          @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                           <div class="form-group">
                            <label for="priorite_id">Priorité</label>
                            <div class="col-sm-10">
                            <select class="form-control @error('priorite_id') is-invalid @enderror" name="priorite_id" autocomplete="priorite_id" autofocus>
                              <option value="{{ old('priorite_id') }}">Selectionner</option>
                               @foreach($priorites as $priorite)
                            <option value="{{ $priorite->id }}" >
                              {{ $priorite->libelle }}</option>

                         @endforeach                         
                            </select>
                          </div>
                            
                             @error('priorite_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div> 

                          <div class="form-group">
                            <label for="expediteur_id">Expéditeur</label>
                            <div class="col-sm-10">
                            <select class="form-control select2 @error('expediteur_id') is-invalid @enderror" name="expediteur_id"  autocomplete="expediteur_id" autofocus>
                              <option value="{{ old('expediteur_id') }}">Selectionner</option>
                              @foreach($userse as $user)                           
                              
                                  <option value="{{ $user->id }}" >
                                    {{ $user->nom }} {{ $user->prenom }}</option>
                              
                               @endforeach                         
                            </select>
                          </div>
                             @error('expediteur_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                              @enderror
                          </div> 


                          <div class="form-group">
                            <nav class="btn btn-primary" id="bnewexpediteur"><i class="fa fa-plus"></i>Ajouter un nouvel expéditeur</nav>
                            <div class="col-sm-10" style="display: none;" id="dnewexpediteur">
                            <label for="newexpediteur">Ajouter un nouvel expéditeur</label>
                            <input class="form-control" name="newexpediteur" id="newexpediteur">
                          </div>
                          </div>

                       <div class="form-group">
                        <label for="destinataire_id">Destinataire</label>
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('destinataire_id') is-invalid @enderror" name="destinataire_id" autocomplete="destinataire_id" autofocus>
                          <option value="{{ old('destinataire_id') }}">Selectionner</option>
                         @foreach($userse as $user)
                                  <option value="{{ $user->id }}" >
                                    {{ $user->nom }} {{ $user->prenom }}</option>
                               @endforeach  
                        </select>
                      </div>
                        
                         @error('destinataire_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>

                      <div class="form-group">
                        <nav class="btn btn-primary" id="bnewdestinataire"><i class="fa fa-plus"></i>Ajouter un nouveau destinataire</nav>
                        <div class="col-sm-10" style="display: none;" id="dnewdestinataire">
                        <label for="newdestinataire">Ajouter un nouveau destinataire</label>
                        <input class="form-control" name="newdestinataire" id="newdestinataire">
                      </div>
                      </div>
                        @endif
                        @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                      <div class="form-group" hidden="hidden">
                        <div class="col-sm-10">
                        <select class="form-control select2 @error('etat_id') is-invalid @enderror" name="etat_id"  autocomplete="etat_id" autofocus>
                          <option value="{{ old('etat_id') }}">Selectionner</option>
                         @foreach($etats as $etat)
                          @if($etat->libelle == "Non Affecté")
                                  <option  selected="selected" value="{{ $etat->id }}" ></option>
                          @endif
                               @endforeach 

                        </select>
                      </div>
                         @error('destinataire_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>
                      @endif
                     <div class="form-group">
                        <label for="courrriern">Document Numérisé</label>
                        <div class="col-sm-10">
                        <input type="file" class="form-control @error('courrriern') is-invalid @enderror" name="courrriern" value="{{ old('image') }}"  autocomplete="courrriern" autofocus>                                                
                      </div>
                        
                         @error('courrriern')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>  


                      <div class="form-group">
                        <label for="images">Pièces jointes</label>
                        <div class="col-sm-10">
                        <input type="file" class="form-control @error('images') is-invalid @enderror" name="images[]" value="{{ old('image') }}"  autocomplete="image" autofocus multiple="multiple">                                                
                      </div>
                        
                         @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                              
                        </div>
                        @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                        <div class="container">
                          <div class="form-group">
                        <label for="reference">Mots Clés</label>
                        <div class="col-sm-10">
                        <input class="form-control @error('motcles') is-invalid @enderror" type="text" name="motcles" value="{{ old('reference') }}" autocomplete="motcles" autofocus placeholder="ajoutez des mots Clés pour une recherche plus facile">
                         
                      </div>
                        
                         @error('motcles')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div> 
                        </div>
                       @endif
                      <div class="container" style="max-width: 200px;">
                          <input type="submit" class="btn btn-primary form-control" value="Ajouter">
                      </div>
                    </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
       

@endsection
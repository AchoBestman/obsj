@extends('layouts.app1')
@section('main')
<br><br>
@inject('role', 'App\Helpers\Helpers')
@inject('piecesjointes', 'App\Helpers\Helpers')
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card" >
              <div style="margin:50px;">
               
           
                 @foreach($courriers as $courrier)
                 <h2 style="text-align: center;">Document N° <em style="color: red">{{ $courrier->numeroEnregistrement }}</em>
                  <p>***********</p></h2>
                
      
             
            <div class="card">
              <div class="card-header" style="background-color: #007bff;">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Détails
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <dl>
                      @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                      <dt>Numéro Primaire</dt>
                      <dd>{{ $courrier->numeroPrimaire }}</dd>
                      @endif
                      <dt>Numéro D'enregistrement</dt>
                      <dd>{{ $courrier->numeroEnregistrement }}</dd>
                      <dt>Référence</dt>
                      <dd>{{ $courrier->reference }}</dd>
                      <dt>Anotation</dt>
                      <dd>{{ $courrier->annotations }}</dd>
                      <dt>Objet</dt>
                      <dd>{{ $courrier->objet }}</dd>
                      <dt>Confidentiel</dt>
                      <dd>{{ $courrier->confidentiel }}</dd>
                      <dt>Type de document</dt>
                      <dd>{{ $courrier->type?$courrier->type->libelle:'' }}</dd>
                      <dt>Document numérisé</dt>
                      <dd><a href="/images/{{ $courrier->courriersNumerise }}" target="_blanck" download="">{{ 'Télécharger' }}</a></dd>
                      <dt>Pièces jointes</dt>
                      <dd>
                      @foreach($piecesjointes::pieces_jointes($courrier->id) as $pieces)
                      <a href="/images/{{ $pieces->libelle }}" title="{{ $pieces->libelle }}" target="_blanck" style="margin-left: 13px;" download="">{{ 'Télécharger' }}</a>
                      @endforeach
                    </dd> 
                    </dl> 
                  </div>
                  <div class="col-md-6">
                    <dl>
                      <dt>Nature</dt>
                      <dd>{{ $courrier->type?$courrier->nature->libelle:'' }}</dd>
                      @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                      <dt>Priorité</dt>
                      <dd>{{ $courrier->type?$courrier->priorite->libelle:'' }}</dd>
                      @endif
                      <dt>Chrono</dt>
                      <dd>{{ $courrier->type?$courrier->chrono->libelle:'' }}</dd>
                      @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                      <dt>Expéditeur</dt>
                      <dd>{{  $courrier->expediteur?$courrier->expediteur->nom:'' }} {{ $courrier->expediteur?$courrier->expediteur->prenom:'' }}</dd>
                      <dt>Destinataire</dt>
                      <dd>{{ $courrier->expediteur?$courrier->destinataire->nom:'' }} {{ $courrier->expediteur?$courrier->destinataire->prenom:'' }}</dd>
                      @endif
                      <dt>Date d'enregistrement</dt>
                      <dd>{{ $courrier->created_at }}</dd>
                      @if($role::role(Auth::user()->role_id) != "mp" &&  $role::role(Auth::user()->role_id) != "comp")
                      <dt>État de traitement</dt>
                      <dd>{{ $courrier->etat?$courrier->etat->libelle:'' }}</dd>
                      <dt>Archive</dt>
                      <dd>@if ($courrier->archive == 0) 
                        {{"Pas encore Archivé"}}
                      @else
                           <em>Archivé le</em>  {{$courrier->datearchive}}
                        @endif
                      </dd>
                      @endif
                    </dl>
                  </div>
                </div>
                
              </div>
               <!-- <a href="{{route('Courrier.index')}}" class="btn btn-primary"> <<< Retour à la liste des courriers</a>-->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
         @endforeach
  
            </div>
            </div>
            
            <!-- /.nav-tabs-custom -->
          </div>
@endsection


@extends('layouts.app1')
@section('main')
<br><br>
@inject('piecesjointes', 'App\Helpers\Helpers')
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card" >
              <div style="margin:50px;">
               
           
                 @foreach($courriers as $courrier)
                 <h2 style="text-align: center;">Courrier N° <em style="color: red">{{ $courrier->numeroEnregistrement }}</em>
                  <p>***********</p></h2>
                
      
             
            <div class="card">
              <div class="card-header" style="background-color: #007bff;">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Archiver un courrier
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                 <form method="POST" action="{{ route('Archivage.store') }}" enctype="multipart/form-data">
                   @csrf
                 <div class="form-group ">
                        <input type="hidden" name="numerocourrier" value="{{ $courrier->numeroEnregistrement }}">
                         <input type="hidden" name="idcour" value="{{ $courrier->id }}">
                        <label for="inputName2" >Quel type d'archivage voulez-vous effectuer ?</label>
                        <div class="">
                        <select class="form-control select2 @error('typearchi') is-invalid @enderror" name="typearchi" value="{{ old('typearchi') }}" required autocomplete="typearchi" autofocus data-placeholder="Selectionner">

                        <option value="1">Semi-archive</option>
                        <option value="2">Archive définitive</option>
                        </select>
                      </div>
                        
                         @error('typearchi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>
                       <div class="form-group">
                        <label for="inputName2">Dans quel dossier voulez-vous le mettre ?</label>
                        <div class="">
                        <select class="form-control select2 @error('chrono_id') is-invalid @enderror" name="chrono_id" value="{{ old('chrono_id') }}" required autocomplete="chrono_id" autofocus>
                          <option>Selectionner</option>
                         @foreach($chronos as $chrono)
                            <option value="{{ $chrono->id }}" >
                              {{ $chrono->libelle }}</option>

                         @endforeach
                        </select>
                      </div>
                        
                         @error('chrono_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>      
                      <div class="form-group ">
                        <label for="inputName" >Date d'expiration</label>                                          
                        <div class="input-group">
                          <input type="date" class="form-control float-right  @error('datedebut') is-invalid @enderror" value="{{ old('datefinarchi') }}" autocomplete="datefinarchi" autofocus  id="datetimepicker" name="datefinarchi" id="reservationdate">
                          
                             @error('datefinarchi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                             @enderror
                          
                          </div>
                        </div>     
                         <div class="form-group ">
                        <label for="inputName" >Note (facultatif)</label>
                        <div class="">
                          <textarea class="form-control @error('instructions') is-invalid @enderror" id="inputName" name="instructions" value="{{ old('intructions') }}"  autocomplete="instructions" autofocus></textarea>
                        </div>
                         @error('instructions')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                      </div> 
                
                      <div class="form-group row">
                       <div class="container">
                        
                           <input type="submit" class="btn btn-primary form-control" value="Procéder à l'archivage">
                      </div>
                      </div> 
                    </form>
                </div>
                
              </div>
               <!-- <a href="{{route('Courrier.index')}}" class="btn btn-primary"> <<< Retour à la liste des courriers</a>-->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
         @endforeach
  
            </div>
            </div>
            
            <!-- /.nav-tabs-custom -->
          </div>
@endsection


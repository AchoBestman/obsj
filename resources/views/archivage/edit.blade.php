@extends('layouts.app1')
@section('main')
<br><br>
@inject('piecesjointes', 'App\Helpers\Helpers')
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card" >
              <div style="margin:50px;">
               
           
                
             
            <div class="card">
              <div class="card-header" style="background-color: #007bff;">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Prolonger la durée du courrier
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                 @foreach($archivages as $archivage)
                 <form method="POST" action="{{ route('Archivage.update', $archivage->id) }}" enctype="multipart/form-data">
                   @csrf

                 <div class="form-group ">
                        
                       
                      <div class="form-group ">
                        <label for="inputName" >Date d'expiration</label>                                          
                        <div class="input-group">
                          <input type="date" class="form-control float-right  @error('datedebut') is-invalid @enderror" value="{{ $archivage->datefinarchi }}" autocomplete="datefinarchi" autofocus  id="datetimepicker" name="datefinarchi" id="reservationdate">
                          
                             @error('datefinarchi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                             @enderror
                          
                          </div>
                        </div>     
                         <div class="form-group ">
                        <label for="inputName" >Note (facultatif)</label>
                        <div class="">
                          <textarea class="form-control @error('instructions') is-invalid @enderror" id="inputName" name="instructions" value="{{ $archivage->instructions}}"  autocomplete="instructions" autofocus></textarea>
                        </div>
                         @error('instructions')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                      </div> 
                
                      <div class="form-group row">
                       <div class="container">
                        
                           <input type="submit" class="btn btn-primary form-control" value="Mettre à jour">
                      </div>
                      </div> 
                      {{ method_field('PUT') }}
                    </form>
                    @endforeach
                </div>
                
              </div>
               <!-- <a href="{{route('Courrier.index')}}" class="btn btn-primary"> <<< Retour à la liste des courriers</a>-->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
       
            </div>
            </div>
            
            <!-- /.nav-tabs-custom -->
          </div>
@endsection


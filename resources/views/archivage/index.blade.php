@extends('layouts.app1')
@inject('role', 'App\Helpers\Helpers')
@inject('users', 'App\Helpers\Helpers')
@inject('role', 'App\Helpers\Helpers')
@section('main')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">

                 
                  
                  
                   <li class="nav-item"><a class="nav-link active" href="#archive" data-toggle="tab" >Dossiers clos</a></li>
                   <li class="nav-item"><a class="nav-link " href="#archivesemi" data-toggle="tab" >Semi-Archive</a></li>
                   <li class="nav-item"><a class="nav-link" href="#archivedef" data-toggle="tab" >Archives définitif</a></li>
                   
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

     
   

                                                  <!-- /.tab-pane -->
                  <div class="tab-pane active" id="archive">
             <div class="table-responsive">
               <table id="example4" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                       <th>Actions</th>  
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                      
                      <th>Dossier clos le</th>
                                     
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                       <th>Actions</th> 
                      <th>Numeros</th>
                      <th>Références</th>                     
                      <th>Objet</th>
                      <th>Dates d'enreg</th>
                      <th>Type</th>
                      <th>Nature</th>
                     
                      <th>Dossier clos le</th>
                          
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($courriers as $courrier)
                     @if($courrier->archive == 1 && $courrier->semiarchive == 0)
                    <tr>
                       <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                        <div class="dropdown-menu">
                             <a class="dropdown-item" href="{{ route('Courrier.show', $courrier->id) }}">Consulter</a> 
                             <a  class="dropdown-item" href="{{ route('Archivage.show', $courrier->id) }}">Archiver</a>
                             <!--  <form method="P" action="{{ route('archive', $courrier->id) }}">
                             @csrf
                             
                             <input type="hidden" name="archive" value="{{0}}">
                             <button  class="dropdown-item"> Désarchiver</button> 
                           </form>    -->                   
                           </div></div></td>
                      <td> {{ $courrier->numeroEnregistrement}}</td>
                      <td> {{ $courrier->reference }}</td>
                      <td> {{ $courrier->objet }}</td> 
                      <td> {{ $courrier->created_at }}</td>
                      <td> {{ $courrier->type?$courrier->type->libelle:'' }}</td>
                      <td> {{ $courrier->nature?$courrier->nature->libelle:'' }}</td>
                     
                      <td> {{$courrier->datearchive }}</td>
                     
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
                  </div>
                                <!-- /.tab-pane -->
            <div class="tab-pane" id="archivesemi">
             <div class="table-responsive">
               <table id="example3" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                       <th>Actions</th>  
                      <th>Références</th>                     
                      <th>Notes</th>
                      <th>Dates d'archivage</th>
                       <th>Dates d'expiration du délai de vie</th>
                                     
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                     <th>Actions</th>  
                      <th>Références</th>                     
                      <th>Notes</th>
                      <th>Dates d'archivage</th>
                       <th>Dates d'expiration du délai de vie</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($archivage as $archivages)
                    @if($archivages->typearchi == 1 && $archivages->corbeille == 0)
                    <tr>
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <form method="GET" action="{{ route('archivedef', $archivages->numero) }}">
                             @csrf
                             <input type="hidden" name="archivedef" value="{{2}}">
                            <button  class="dropdown-item"> Archiver définitivement</button> 
                           </form>    
                            <a class="dropdown-item" href="{{ route('remettre', $archivages->numero) }}">Remettre en ligne</a>
                            <a class="dropdown-item" href="{{ route('Archivage.edit', $archivages->id) }}">Prolonger la durée</a>
                          </div>
                         
                        </div></td>   
                      <td> {{ $archivages->numero}}</td>
                      <td> {{ $archivages->instructions }}</td> 
                      <td> {{ $archivages->created_at }}</td> 
                      <td> {{ $archivages->datefinarchi }}</td>
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
             </div>

                                                  <!-- /.tab-pane -->
                 <div class="tab-pane" id="archivedef">
             <div class="table-responsive">
               <table id="example3" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                       <th>Actions</th>  
                      <th>Références</th>                     
                      <th>Notes</th>
                      <th>Dates d'archivage</th>
                       <th>Dates d'expiration du délai de vie</th>
                                     
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                     <th>Actions</th>  
                      <th>Références</th>                     
                      <th>Notes</th>
                      <th>Dates d'archivage</th>
                       <th>Dates d'expiration du délai de vie</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($archivage as $archivages)
                    @if($archivages->typearchi == 2 && $archivages->corbeille == 0)
                    <tr>
                      <td><div class="btn-group">
                          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Options
                          </button>
                          <div class="dropdown-menu">
                            <form method="GET" action="{{ route('delete', $archivages->id) }}">
                             @csrf
                            <button  class="dropdown-item">Supprimer</button> 
                           </form>
                        
                          </div>
                        </div></td>   
                      <td> {{ $archivages->numero}}</td>
                        <td> {{ $archivages->instructions }}</td> 
                        <td> {{ $archivages->created_at }}</td> 
                        <td> {{ $archivages->datefinarchi }}</td>
                    </tr>
                    @endif
                    @endforeach            
                  </tbody>
                </table>
              </div>
             </div>


                  
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
       

@endsection
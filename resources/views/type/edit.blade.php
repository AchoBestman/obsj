@extends('layouts.app1')
@section('main')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Modification de type</a></li>                 
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <!-- /.tab-pane -->
              <div class="active tab-pane" id="timeline">
                @foreach($types as $type)
                <form class="form-horizontal" method="POST" action="{{ route('Type.update', $type->id) }}">
                       @csrf
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Libellé</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control @error('libelle') is-invalid @enderror" id="libelle" placeholder="entrez un type" name="libelle" value="{{ $type->libelle }}" required autocomplete="libelle" autofocus>
                        </div>
                         @error('libelle')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror
                      </div>                      
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Abréviation</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control @error('abrev') is-invalid @enderror" id="inputName2" placeholder="Name" name="abrev" value="{{$type->abrev }}" required autocomplete="abrev" autofocus>
                        </div>
                         @error('abrev')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Modifer</button>
                        </div>
                      </div>
                     {{ method_field('PUT') }}
                    </form>
                    @endforeach
                  </div>
               
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
@endsection
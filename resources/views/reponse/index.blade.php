@extends('layouts.app1')
@section('main')
@inject('reponse', 'App\Helpers\Helpers')
@inject('delaitraitement', 'App\Helpers\Helpers')
<br><br>
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card" >
              <div style="margin:50px;">
               
           
                <h6 style="color: green;">Informations du courrier</h6><br> 
                
              <div class="row" >
                 @if($affectation)
                <div class="col-md-4">
                 
                  <label>Référence courrier: {{ $affectation->courrier?$affectation->courrier->reference:'' }}<em style="color: red;"></em></label><br>

                
                  <label>Note:</label>
                  <textarea  name="" class="form-control">{{ $affectation->intructions }}</textarea>
                </div>

                <div class="col-md-4">
                  
                 <label> Priorité: <em> {{ $affectation->courrier?$affectation->courrier->priorite->libelle:'' }}</em></label><br>

                 <label>Objet: </label><br>
                   <textarea  name="" class="form-control">{{ $affectation->courrier?$affectation->courrier->objet:'' }}</textarea>
                </div>

                <div class="col-md-4">
                  <label>Date de dépot: <em> {{ $affectation->courrier?$affectation->courrier->created_at:'' }}</em></label><br>

                  <label>Confidentiel: <em>{{ $affectation->courrier?$affectation->courrier->confidentiel:'' }}</em></label><br>
                    <label>Annotation: <em> {{ $affectation->courrier?$affectation->courrier->annotations:'' }}</em></label><br>

                  <label>Délai restant: <em>{{$delaitraitement::delaitraitementFini($affectation->delaiTraitementF, $affectation->delaiTraitementD)}}</em> </label><br>
                  <label>Instructions: <em> {{ $affectation->inst}}</em></label><br>
                </div>
                <div class="col-md-4">
                  <h5>Version numérique associée</h5>
                   <a href="/storage/{{ $affectation->courrier->courriersNumerise }}" target="_blanck">{{ 'Télécharger' }}</a>
                </div>
                <div class="col-md-8">
                  <h5>Listes des pièces jointes</h5>
                   @foreach($reponse::pieces_jointes1($affectation->courrier->id,$affectation->id) as $pieces)
                      <a href="/storage/{{ $pieces->libelle }}" title="{{ $pieces->libelle }}" target="_blanck" style="margin-left: 13px;">{{ 'Télécharger' }}</a>
                      @endforeach
                </div>
                 @endif
                
              </div>
             
              <br><br>

              <form method="POST" action="{{ route('Reponse.store') }}" enctype="multipart/form-data">  
                  @csrf
           <div class="">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Écrire une Réponse</h3>
                
                  <div class="form-group" style="float: right;">
                        <div class="" style="width: 200px;">
                        <select class="form-control select2 @error('etat_id') is-invalid @enderror" name="etat_id"  autocomplete="etat_id" autofocus>
                          <option value="{{ old('etat_id')?? $affectation->etat_id}}" >État de traitement</option>
                         @foreach($etats as $etat)                         
                          @if($etat->libelle !== "Non Affecté" && $etat->libelle !== "Close")
                                  <option value="{{ $etat->id }}" >{{$etat->libelle}}</option>
                          @endif
                               @endforeach 

                        </select>
                        
                      </div>
                         @error('destinataire_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                         
                      </div>

                 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                 <input type="hidden" name="affectation_id" value="{{$affectation->id}}">
                <div class="form-group">
                    <textarea {{\Auth::user()->id == $affectation->qui? 'readonly': ''}} id="compose-textarea" name="libelle" class="form-control">
                      {{$reponse::reponse($affectation->id)? $reponse::reponse($affectation->id)->libelle : old('libelle')}}
                    </textarea>
                </div>
                <div class="form-group">
                  
                    <input type="file" name="images[]" multiple="multiple">
                  
                  <p class="help-block">Max. 32MB</p>
                </div>
               
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="float-right">
                  @if(\Auth::user()->id !== $affectation->qui)
                  <button  class="btn btn-primary">Transmettre</button>
                  @else
                  <button  class="btn btn-primary">Envoyer</button>
                  @endif
                </div>
                
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
              </form>
               
            </div>
            </div>
            
            <!-- /.nav-tabs-custom -->
          </div>
@endsection


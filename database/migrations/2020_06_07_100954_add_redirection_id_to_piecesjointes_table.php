<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRedirectionIdToPiecesjointesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('piecesjointes', function (Blueprint $table) {
            $table->foreignId('redirection_id')->nullable()
            ->constrained('redirections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('piecesjointes', function (Blueprint $table) {
            Schema::dropIfExists('redirection_id');
        });
    }
}

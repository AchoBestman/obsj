<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedirectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redirections', function (Blueprint $table) {
            $table->id();
            $table->foreignId('affectation_id')->nullable()
            ->constrained('affectations')->onDelete('cascade');
           $table->foreignId('qui')->nullable()->constrained('users')->onDelete('cascade');
            $table->foreignId('aqui')->nullable()->constrained('users')->onDelete('cascade');
            $table->string('instructions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redirections');
    }
}

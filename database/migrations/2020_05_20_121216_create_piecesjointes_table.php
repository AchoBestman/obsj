<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiecesjointesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piecesjointes', function (Blueprint $table) {
           $table->id();
             $table->foreignId('courrier_id')->nullable()
            ->constrained('courriers')->onDelete('cascade');
            $table->foreignId('affectation_id')->nullable()
            ->constrained('affectations')->onDelete('cascade');
            $table->string('libelle')->nullable();
            $table->string('abrev')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piecesjointes');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReponseIdToPiecesjointesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('piecesjointes', function (Blueprint $table) {
            $table->foreignId('reponse_id')->nullable()
            ->constrained('reponses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('piecesjointes', function (Blueprint $table) {
            Schema::dropIfExists('reponse_id');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectionsTSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directions_t_s', function (Blueprint $table) {
            $table->id();
            $table->foreignId('direction_id')->nullable()
            ->constrained('directions')->onDelete('cascade');
            $table->string('libelle')->nullable();
            $table->string('abrev')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directions_t_s');
    }
}

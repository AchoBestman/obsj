<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courriers', function (Blueprint $table) {
             $table->id();
             $table->foreignId('type_id')->nullable()
            ->constrained('types')->onDelete('cascade');
             $table->foreignId('nature_id')->nullable()
            ->constrained('natures')->onDelete('cascade');
             $table->foreignId('priorite_id')->nullable()
            ->constrained('priorites')->onDelete('cascade');
             $table->foreignId('user_id')->nullable()
            ->constrained('users')->onDelete('cascade');
             $table->foreignId('etat_id')->nullable()
            ->constrained('etats')->onDelete('cascade');
             $table->foreignId('chrono_id')->nullable()
            ->constrained('chronos')->onDelete('cascade');
             $table->foreignId('expediteur_id')->nullable()
            ->constrained('users')->onDelete('cascade');
             $table->foreignId('destinataire_id')->nullable()
            ->constrained('users')->onDelete('cascade');            
            $table->string('confidentiel')->nullable();
            $table->string('numeroEnregistrement')->nullable();
            $table->string('numeroPrimaire')->nullable();
            $table->string('annotations')->nullable();
            $table->string('objet')->nullable();
            $table->string('reference')->nullable();
            $table->string('courriersNumerise')->nullable();           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courriers');
    }
}

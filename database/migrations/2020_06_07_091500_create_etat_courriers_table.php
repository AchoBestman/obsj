<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtatCourriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etat_courriers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('etat_id')->nullable()
            ->constrained('etats')->onDelete('cascade');
            $table->foreignId('courrier_id')->nullable()
            ->constrained('courriers')->onDelete('cascade');
             $table->foreignId('affectation_id')->nullable()
            ->constrained('affectations')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etat_courriers');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffectationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affectations', function (Blueprint $table) {
            $table->id();
              $table->foreignId('courrier_id')->nullable()
            ->constrained('courriers')->onDelete('cascade');             
            $table->foreignId('qui')->nullable()->constrained('users')->onDelete('cascade');
            $table->foreignId('aqui')->nullable()->constrained('users')->onDelete('cascade');
            $table->foreignId('etat_id')->nullable()->constrained('etats')->onDelete('cascade');
            $table->string('intructions')->nullable(); 
            $table->datetime('delaiTraitementD')->nullable(); 
            $table->datetime('delaiTraitementF')->nullable();        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affectations');
    }
}

Vous pouvez également télécharger des fichiers existants depuis votre ordinateur en suivant les instructions ci-dessous.


Configuration globale de Git
git config --global user.name "AIKPE ACHILE"
git config --global user.email " aikpeachille55@gmail.com "

Créer un nouveau référentiel
git clone https://gitlab.com/AchoBestman/maep_courrier.git
cd maep_courrier
touchez README.md
git add README.md
git commit -m "ajouter README"
git push -u origin master

Pousser un dossier existant
cd dossier_existant
git init
git remote add origin https://gitlab.com/AchoBestman/maep_courrier.git
git add.
git commit -m "Commit initial"
git push -u origin master

Pousser un référentiel Git existant
cd existant_repo
git remote renommer l'origine ancienne-origine
git remote add origin https://gitlab.com/AchoBestman/maep_courrier.git 
git push -u origin --all
git push -u origin --tags